package phonetoaster.com.bubbletranslate.container;

import android.content.Context;

import com.txusballesteros.bubbles.BubbleLayout;
import com.txusballesteros.bubbles.BubblesManager;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import phonetoaster.com.bubbletranslate.helper.AppDatabase;


public class Container {

    private static final Container INSTANCE = new Container();//TODO: CHANGE TO REAL SINGLETON

    // Common
    private BubbleLayout bubbleLayout;
    private BubblesManager bubblesManager;
    private AppDatabase mAppDatabase;
    private Context mContext;

    private Integer mExerciseSessionId;

    public static Container getInstance() {
        return INSTANCE;
    }

    public BubbleLayout getBubbleLayout() {
        return bubbleLayout;
    }

    public void setBubbleLayout(BubbleLayout bubbleLayout) {
        this.bubbleLayout = bubbleLayout;
    }

    public BubblesManager getBubblesManager() {
        return bubblesManager;
    }

    public void setBubblesManager(BubblesManager bubblesManager) {
        this.bubblesManager = bubblesManager;
    }

    public void setDatabaseManager(AppDatabase databaseManager) {
        this.mAppDatabase = databaseManager;
    }

    public AppDatabase getDatabaseManager() {
        if (mAppDatabase == null){
            AppDatabase appDatabase = AppDatabase.getAppDatabase(getmContext());
            this.setDatabaseManager(appDatabase);
        }
        return mAppDatabase;
    }

    public Context getmContext() { //TODO: SET CONTAXT AFTER THE START OF THE APP
        return mContext;
    }

    public void setmContext(Context mContext) {
        this.mContext = mContext;
    }

    public Integer getmExerciseSessionId() {
        return mExerciseSessionId;
    }

    public void setmExerciseSessionId(Integer mExerciseSessionId) {
        this.mExerciseSessionId = mExerciseSessionId;
    }

    public void resetExerciseSessionId() {
        this.mExerciseSessionId = null;
    }


}

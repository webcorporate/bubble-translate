package phonetoaster.com.bubbletranslate.activity;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.blankj.utilcode.util.Utils;
import com.facebook.stetho.Stetho;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;

import java.util.List;

import phonetoaster.com.bubbletranslate.R;
import phonetoaster.com.bubbletranslate.common.Constants;
import phonetoaster.com.bubbletranslate.container.Container;
import phonetoaster.com.bubbletranslate.helper.AllListenerHelper;
import phonetoaster.com.bubbletranslate.helper.CommonHelper;
import phonetoaster.com.bubbletranslate.helper.ToastHelper;
import phonetoaster.com.bubbletranslate.model.Preference;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class BaseActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {

    private static final String[] REQUIRED_PERMISSIONS = {
            Manifest.permission.SYSTEM_ALERT_WINDOW,
            //Manifest.permission.VIBRATE, //IS IT NORMAL PERMISSION GRANTED BY DEFAULT
            //Manifest.permission.RECEIVE_BOOT_COMPLETED, //IS IT NORMAL PERMISSION GRANTED BY DEFAULT
            //Manifest.permission.ACCESS_NETWORK_STATE, //IS IT NORMAL PERMISSION GRANTED BY DEFAULT
    };
    private BroadcastReceiver mBroadcastReceiverGlobalEvent;
    private boolean broadcastRegistred;


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Logger.d("onRequestPermissionsResult");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        // Some permissions have been granted
        Logger.d("onPermissionsGranted");
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        // Some permissions have been denied
        Logger.d("onPermissionsDenied");
        ToastHelper.showToastMessage(this, getString(R.string.without_permission_cannot_display_bubble_message), Constants.Global.DEBUG);
    }

    //THIS METHOD SOLVING CAUSING THE ISSUE ON API >= 23 https://stackoverflow.com/questions/32224452/android-unable-to-add-window-permission-denied-for-this-window-type
    @AfterPermissionGranted(Constants.Permission.SYSTEM_ALERT_WINDOW_CODE)
    private void requestEasyPermissions() throws RuntimeException {
        Logger.d("requestPermissions");
        EasyPermissions.requestPermissions(
                this,
                "Description",
                Constants.Permission.SYSTEM_ALERT_WINDOW_CODE,
                REQUIRED_PERMISSIONS
        );
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initCommon();
    }

    @Override
    protected void onResume() {
        super.onResume();
        udpateDataInView();
        this.registerDataRefreshEvent();
    }

    @Override
    protected void onStop() {
        super.onStop();  // Always call the superclass method first
        if(mBroadcastReceiverGlobalEvent != null && broadcastRegistred)
            this.unregisterDataRefreshEvent();
            broadcastRegistred = false;
    }

    private void initCommon() {
        Container.getInstance().setmContext(this);
        if(Constants.Global.DEBUG) {
            initDebug();
        }
        this.setAppVersion();
        this.initServices();
        this.registerDataRefreshEvent();
        this.checkPermission();
        this.checkAutostartPermission();
    }

    private void initDebug() { //TODO: MOVE TO BASE ACTIVITY OR HELPER
        Logger.addLogAdapter(new AndroidLogAdapter());
        Stetho.initializeWithDefaults(this);
        Utils.init(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Logger.d("onActivityResult with result code: " +resultCode);
        if (requestCode == Constants.Permission.MANAGE_OVERLAY_PERMISSION_CODE) {
            if (CommonHelper.canAppDrawOverlays(this)) {
                Logger.d("You have permission");
                ToastHelper.showToastMessage(this, this.getString(R.string.permission_granted), Constants.Global.DEBUG);
            } else {
                setNotificationType(Constants.TranslationMode.NOTIFICATION);
            }
        }
    }

    private void checkPermission() {
        if(CommonHelper.shouldAskPermission()
                && !CommonHelper.canAppDrawOverlays(this)
                && CommonHelper.getNotificationType(this).equals(Constants.TranslationMode.FLOATING_WINDOW)) {
            Logger.d("Cannot Go");
            this.showPermissionAlert();
        } else {
            Logger.d("Can Go");
        }
    }

    private void checkAutostartPermission() {
        CommonHelper.checkAutostartPermission(this);
    }

    private void showPermissionAlert() throws RuntimeException {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(getString(R.string.permission_required_overlay_desc));
        builder1.setCancelable(false);

        builder1.setPositiveButton(
                getString(R.string.allow),
                new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    public void onClick(DialogInterface dialog, int id) {
                        //requestEasyPermissions();
                        requestOverlayPerm(); //TODO: IS POSSIBLE TO REQUEST OVERLAY PERMISSION
                        // VIA EASYPERMISSIONS?
                    }
                });

        builder1.setNegativeButton(
                getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ToastHelper.showToastMessage(getBaseContext(), R.string.translations_will_be_displayed_using_notification, Constants.Global.DEBUG);
                        setNotificationType(Constants.TranslationMode.NOTIFICATION);
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();

    }

    private void initServices() {
        Logger.d("initService");
        AllListenerHelper.initializeListeners(this);
    }

    private void requestOverlayPerm() {
        Intent intent = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            Uri URI = Uri.parse(Constants.Global.PACKAGE_PROTOCOL+getPackageName());
            intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, URI);
            startActivityForResult(intent, Constants.Permission.MANAGE_OVERLAY_PERMISSION_CODE);
        }
    }

    private void setNotificationType(String notificationType) {
        Logger.d("setTranslationMode");
        if(notificationType.equals(Constants.TranslationMode.NOTIFICATION)) {
            Preference.getInstance(this).setTranslationType(Constants.TranslationMode.NOTIFICATION);
            ToastHelper.showToastMessage(this, this.getString(R.string.translation_mode_set_to_notification_bar), Constants.Global.DEBUG);
        }

        PreferenceActivity.Display d = new PreferenceActivity.Display();//TODO: HOW TO REFRESH SETTINGS
        d.refresh();
    }

    private void setAppVersion() {
        Logger.d("setAppVersion");
        Preference.getInstance(this).setAppVersion(Constants.Global.APP_VERSION);
        PreferenceActivity.Display d = new PreferenceActivity.Display();//TODO: HOW TO REFRESH SETTINGS
        d.refresh();
    }

    private void unregisterDataRefreshEvent() {
        if(mBroadcastReceiverGlobalEvent != null)
            unregisterReceiver(mBroadcastReceiverGlobalEvent);
    }

    private void registerDataRefreshEvent() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.Broadcast.GLOBAL_UPDATE_EVENT);
        mBroadcastReceiverGlobalEvent = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Logger.i("actualizeDataOnEvent");
                udpateDataInView();
            }
        };
        this.registerReceiver(mBroadcastReceiverGlobalEvent, intentFilter);
        broadcastRegistred = true;
    }

    public void udpateDataInView() {

    }
}

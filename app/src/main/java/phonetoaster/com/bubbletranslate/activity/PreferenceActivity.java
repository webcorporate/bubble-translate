package phonetoaster.com.bubbletranslate.activity;


import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import com.orhanobut.logger.Logger;

import phonetoaster.com.bubbletranslate.R;
import phonetoaster.com.bubbletranslate.common.Constants;
import phonetoaster.com.bubbletranslate.helper.ExceptionLogHelper;
import phonetoaster.com.bubbletranslate.helper.ToastHelper;


public class PreferenceActivity extends BaseActivity implements SharedPreferences.OnSharedPreferenceChangeListener  {

    private SharedPreferences mSharedPref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logger.d("onCreate");
        this.initFragmentManager();


        //INIT PREFERENCE CHANGE LISTENER
        this.initCommon();
        this.initPreferenceListener();

        //OLDER WORKING EXAMPLE
        /*
        if(CommonHelper.isAdditionalPermissionsRequired() && !CommonHelper.isAdditionalPermissionsSet(PreferenceActivity.this)){
            //this.showPermissionAlert();
            this.requestPermissions();
        } else if(!CommonHelper.isAdditionalPermissionsRequired() || CommonHelper.isAdditionalPermissionsSet(PreferenceActivity.this)) {
            Logger.d("No additional permissions should be set");
            Intent intent = new Intent(PreferenceActivity.this, Service.class);
            startService(intent);
            AllListenerHelper.initializeListeners(this);
        }
        */
    }

    //TODO:
    private void startServices() {
        Intent intent = new Intent(PreferenceActivity.this, Service.class);
        startService(intent);
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Logger.d("onSharedPreferenceChanged "+key);
        //sharedPreferences.getBoolean(key, false);
        ToastHelper.showToastMessage(this,
                this.getResources().getString(R.string.preferences_saved),
                Constants.Global.DEBUG);
    }

    private void initPreferenceListener() {
        Logger.d("initPreferenceListener");
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);
    }

    private void initFragmentManager() {
        Logger.d("initPreferenceListener");
        if (getFragmentManager().findFragmentById(android.R.id.content)==null) {
            getFragmentManager()
                    .beginTransaction()
                    .add(android.R.id.content, new Display())
                    .commit();
        }
    }

    private void initCommon() {
        Logger.d("initCommon");
        mSharedPref = this.getPreferences(Context.MODE_PRIVATE);
    }

    public static class Display extends PreferenceFragment  {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Logger.d("Display");
            try {
                this.addPreferencesFromResource(R.xml.preferences);
            } catch (Exception e) {
                ExceptionLogHelper.log(e);
            }
        }

        public void refresh() {
            //addPreferencesFromResource(R.xml.preferences);
        }
    }


}

package phonetoaster.com.bubbletranslate.activity;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.List;

import phonetoaster.com.bubbletranslate.R;
import phonetoaster.com.bubbletranslate.container.Container;
import phonetoaster.com.bubbletranslate.fragment.ExerciseResultSlidePageFragment;
import phonetoaster.com.bubbletranslate.fragment.ScreenSlidePageFragment;
import phonetoaster.com.bubbletranslate.helper.AppDatabase;
import phonetoaster.com.bubbletranslate.model.Translation;

public class ExerciseActivity extends BaseActivity {

    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private ViewPager mViewPager;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;

    private List<Translation> mTranslationList = new ArrayList<>();

    private Button mNextTranslationBtn;
    private TextView mProgressTv;
    private AppDatabase appDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise);
        initCommon();
        setListeners();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home){
            if(shouldDisplayDialog())
                showLeavingDialog(this);
            else
                finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private Boolean shouldDisplayDialog(){
        Integer currentPage = mViewPager.getCurrentItem();
        return currentPage > 0;
    }

    private void initCommon() {
        // GET DATA FIRST
        appDb = AppDatabase.getAppDatabase(this); //TODO: MOVE TO SINGLETON
        mTranslationList  = appDb.daoInterfaceTranslation().getAll();

        // INSTANTIATE A VIEWPAGER AND A PAGERADAPTER.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mPagerAdapter);

        setSessionId();
    }

    private void setListeners() {

    }

    public void moveToNextSlide() {
        mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);
    }

    public void moveToFirstSlide() {
        mViewPager.setCurrentItem(0);
    }

    public void setSessionId() {
        // GET & SET EXERCISE SESSION ID
        Integer lastExerciseSessionId = appDb.daoInterfaceExercise().getLastSessionId();
        Container.getInstance().setmExerciseSessionId(lastExerciseSessionId+1);
    }

    public void disableSlide() {
        mViewPager.beginFakeDrag();
    }

    @Override
    public void onBackPressed() {
        Logger.d("onBackPressed");
        if (mViewPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            //mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1);
            if(shouldDisplayDialog())
                showLeavingDialog(this);
            else
                finish();
        }
    }

    private void showLeavingDialog(Context context) {
        new MaterialDialog.Builder(context)
                .title(R.string.leave_current_exercise)
                .content(R.string.your_progress_will_be_lost)
                .positiveText(R.string.ok)
                .negativeText(R.string.cancel)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        finish();
                    }
                })
                .show();
    }

    /**
     * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
     * sequence.
     */
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            if(position < mTranslationList.size()) {
                Translation tr = mTranslationList.get(position);
                fragment = ScreenSlidePageFragment.newInstance(tr, position, getCount() -1 ); //+1 DUE THE RESULT SLIDE
            }
            if(position == mTranslationList.size())
                fragment = ExerciseResultSlidePageFragment.newInstance(position, getCount());
            return fragment;
        }

        @Override
        public int getCount() {
            return mTranslationList.size() + 1; //+1 FOR RESULT SLIDE
        }
    }
}

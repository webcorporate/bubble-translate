package phonetoaster.com.bubbletranslate.activity;


import android.content.Context;
import android.support.animation.DynamicAnimation;
import android.support.animation.FlingAnimation;
import android.support.v4.view.GestureDetectorCompat;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.orhanobut.logger.Logger;

import phonetoaster.com.bubbletranslate.common.Constants;

/*
    Layout used for overriding touch events in child objects of parent layout
    taken from
 */
public class CustomFrameLayout extends FrameLayout implements GestureDetector.OnGestureListener {

    private int initialX = 0;
    private int initialY = 0;
    private float initialTouchX = 0;
    private float initialTouchY = 0;
    private WindowManager.LayoutParams params;
    private WindowManager windowManager;
    private View view;
    private ViewGroup viewGroup;
    private GestureDetectorCompat mDetector;
    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_THRESHOLD_VELOCITY = 150;
    private boolean isReturnAnimation = false;


    public CustomFrameLayout(Context context, WindowManager windowManager) {
        super(context);
        this.windowManager = windowManager;
        mDetector = new GestureDetectorCompat(context,this);
    }

    public void setView(View view) {
        this.view = view;
    }

    public void setParams(WindowManager.LayoutParams params) {
        this.params = params;
    }

    public void setViewGroup(ViewGroup params) {
        this.viewGroup = params;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent e) {
        // do what you need to with the event, and then...
        Logger.d("dispatchTouchEvent");
        // Set the gesture detector as the double tap
        // listener.
        mDetector.onTouchEvent(e);
        return super.dispatchTouchEvent(e);
    }

    @Override
    public boolean onDown(MotionEvent event) {
        Logger.d("onDown");
        initialX = params.x;
        initialY = params.y;
        initialTouchX = event.getRawX();
        initialTouchY = event.getRawY();
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {
        Logger.d("onShowPress");
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        Logger.d("onScroll distanceX: "+distanceX+" distanceY: "+distanceY);
        params.y = initialY + (int) (e2.getRawY() - initialTouchY);
        params.x = 0;
        windowManager.updateViewLayout(viewGroup, params);

        /*
        scrollBy(0, (int) distanceY);
        */

        /*
        params.y = initialY + (int) (e2.getRawY() - initialTouchY);
        params.x = 0;
        windowManager.updateViewLayout(viewGroup, params);
        */
        return true;
    }

    @Override
    public void onLongPress(MotionEvent e) {
        Logger.d("onLongPress");
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        Logger.d("onFling "+ velocityY);
        if(Constants.Global.FLING_ANIMATION_ENABLED) {
            if(e1.getY() - e2.getY() > SWIPE_MIN_DISTANCE) {
                Logger.d("Bottom to top");
                slideView(velocityY, e1, e2, Constants.Global.MOVE_BOTTOM_TOP);
                return true; // Bottom to top
            }  else if (e2.getY() - e1.getY() > SWIPE_MIN_DISTANCE) {
                Logger.d("Top to bottom");
                slideView(velocityY, e1, e2, Constants.Global.MOVE_TOP_BOTTOM);
                return true; // Top to bottom
            }
        }
        return false;
    }

    private void slideView(float velocityY, MotionEvent motionEvent1, final MotionEvent motionEvent2, String direction) {

        FlingAnimation flingAnimation = new FlingAnimation(viewGroup, DynamicAnimation.TRANSLATION_Y); // moves the view on the x-axis
        // derive the pixelPerSecond with start velocity value
        float pixelPerSecond = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, motionEvent1.getY(), getResources().getDisplayMetrics());
        flingAnimation
                .setStartVelocity(velocityY)
                .setFriction(0.5f)
                .start();

    }


}

package phonetoaster.com.bubbletranslate.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.orhanobut.logger.Logger;

import java.util.List;

import phonetoaster.com.bubbletranslate.R;
import phonetoaster.com.bubbletranslate.adapter.LastTranslationAdapter;
import phonetoaster.com.bubbletranslate.common.Constants;
import phonetoaster.com.bubbletranslate.helper.AppDatabase;
import phonetoaster.com.bubbletranslate.helper.CommonHelper;
import phonetoaster.com.bubbletranslate.helper.ExceptionLogHelper;
import phonetoaster.com.bubbletranslate.helper.ToastHelper;
import phonetoaster.com.bubbletranslate.helper.TranslationHelper;
import phonetoaster.com.bubbletranslate.model.Exercise;
import phonetoaster.com.bubbletranslate.model.LastTranslation;
import phonetoaster.com.bubbletranslate.model.Translation;

public class DashBoardActivity extends BaseActivity {

    private Button btnSaved;
    private Context mContext;
    private Integer mCountOfExercises;
    private List<Translation> mTranslationList;
    private List<Exercise> mExerciseList;
    private List<LastTranslation> mLastTranslationList;
    private RecyclerView recyclerView;
    private LinearLayout mSavedPhrasesLayout, mExercisesLayout;
    private LastTranslationAdapter mLastTranslationAdapter;
    private TextView mTranslationCountTv, mExerciseCountTv,  mExerciseLastDateTv, noLastTranslationsTv;
    private AppDatabase appDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);
        initCommon();
        initViews();
        initClickEvents();
        getValues();
        setValues();
    }

    @Override
    protected void onStop() {
        super.onStop();  // Always call the superclass method first
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopServiceClearInstance();
    }

    private void initCommon() {
        mContext = this;
        appDb = AppDatabase.getAppDatabase(mContext);
    }

    private void getValues() {
        mTranslationList  = Translation.getAll();
        mCountOfExercises = Exercise.getCountDistinct();
    }

    private void initViews() {
        mTranslationCountTv = findViewById(R.id.translation_count_tv);
        mExerciseCountTv = findViewById(R.id.exercise_count_tv);
        noLastTranslationsTv = findViewById(R.id.no_available_translations_tv);
        mSavedPhrasesLayout = findViewById(R.id.saved_phrases_layout);
        mExercisesLayout = findViewById(R.id.exercises_layout);
        //mExerciseLastDateTv = findViewById(R.id.last_exercise_date_tv);
    }

    private void initClickEvents() {
        mSavedPhrasesLayout.setOnClickListener(savedPhrasesLayoutClickListener);
        mExercisesLayout.setOnClickListener(exercisesLayoutClickListener);
    }

    private void setValues() {
        mTranslationCountTv.setText(String.valueOf(mTranslationList.size()));
        mExerciseCountTv.setText(String.valueOf(mCountOfExercises));
        this.fillLastTranslationAdapter();
        //mExerciseLastDateTv.setText(mTranslationList.size());
    }

    @Override
    public void udpateDataInView() {
        getValues();
        setValues();
        fillLastTranslationAdapter();
    }

    private void fillLastTranslationAdapter() {
        mLastTranslationList  = appDb.daoInterfaceLastTranslation().getAll();
        recyclerView = (RecyclerView) findViewById(R.id.last_translated_phrases_rv);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        LastTranslationAdapter.OnItemClickListener listener = new LastTranslationAdapter.OnItemClickListener() {
            @Override
            public void onListItemSelected(LastTranslation translation) {
                Logger.d("onListItemSelected");
            }

            @Override
            public void onListItemDeleted(LastTranslation translation) {
                AppDatabase appDb = AppDatabase.getAppDatabase(mContext); //MOVE TO CONTAINER OR SINGLETON
                appDb.daoInterfaceLastTranslation().delete(translation);
            }

            @Override
            public void imageViewOnClick(ImageView v, int position) {
                Logger.d("imageViewOnClick on position "+position);
                LastTranslation translation = mLastTranslationList.get(position);
                try {
                    TranslationHelper.displayTranslation(getApplicationContext(), translation.getSourceText(), translation.getTranslatedText(), false);
                } catch (Exception e) {
                    ExceptionLogHelper.log(e);
                }
            }
        };

        //FILL LAST TRANSLATIONS
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL)); //SET VERTICAL DIVIDER
        if(mTranslationList.size() > 0) {
            mLastTranslationAdapter = new LastTranslationAdapter(mLastTranslationList, listener, mContext);
            recyclerView.setAdapter(mLastTranslationAdapter);
            noLastTranslationsTv.setVisibility(View.GONE);
            mLastTranslationAdapter.notifyDataSetChanged();
        } else {
            noLastTranslationsTv.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);

        }
    }

    private void stopServiceClearInstance(){
        try {
            //unregisterReceiver(null);
        } catch (Exception e) {
            Logger.e(e.getMessage());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.button_action_saved_phrases) {
            Logger.d("button_action_saved_phrases");
            if(Translation.getInstance().getCount() > 0)
                CommonHelper.startActivity(SavedTranslationsActivity.class);
            else
                ToastHelper.showToastMessage(this, R.string.no_saved_phrases, true);
            return true;
        }

        if (id == R.id.button_action_exercise) {
            Logger.d("Exercise");
            if(Translation.getInstance().getCount() > 0)
                CommonHelper.startActivity(ExerciseActivity.class);
            else
                ToastHelper.showToastMessage(this, R.string.no_saved_phrases, true);
            return true;
        }

        if (id == R.id.button_settings) {
            Logger.d("Settings");
            String activityName = PreferenceActivity.class.getSimpleName();
            CommonHelper.navigateToActivity(activityName, mContext);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private View.OnClickListener savedPhrasesLayoutClickListener  = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String activityName = SavedTranslationsActivity.class.getSimpleName();
            CommonHelper.navigateToActivity(activityName, mContext);
        }
    };

    private View.OnClickListener exercisesLayoutClickListener  = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String activityName = ExerciseActivity.class.getSimpleName();
            CommonHelper.navigateToActivity(activityName, mContext);
        }
    };

}

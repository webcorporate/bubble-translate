package phonetoaster.com.bubbletranslate.activity;

import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.daimajia.swipe.SwipeLayout;
import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.List;
import phonetoaster.com.bubbletranslate.R;
import phonetoaster.com.bubbletranslate.adapter.SavedTranslationAdapter;
import phonetoaster.com.bubbletranslate.helper.AppDatabase;
import phonetoaster.com.bubbletranslate.helper.CommonHelper;
import phonetoaster.com.bubbletranslate.helper.ToastHelper;
import phonetoaster.com.bubbletranslate.model.Translation;

public class SavedTranslationsActivity extends BaseActivity {

    private List<Translation> translationList = new ArrayList<>();
    private RecyclerView recyclerView;
    private SavedTranslationAdapter mAdapter;
    private Context mContext;
    private SearchView searchView;
    private SwipeLayout mSwipeLayout;
    private SavedTranslationAdapter.OnItemClickListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_translations);
        mContext = this;
        fillAdapter();
    }

    private void fillAdapter() {
        AppDatabase appDb = AppDatabase.getAppDatabase(this); //TODO: MOVE TO SINGLETON
        translationList  = appDb.daoInterfaceTranslation().getAll();
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_saved_translations);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        listener = new SavedTranslationAdapter.OnItemClickListener() {
            @Override
            public void onListItemSelected(Translation translation) {
                Logger.d("onListItemSelected");
                Logger.d(translation.getSourceText());
            }

            @Override
            public void onListItemDeleted(Translation translation) {
                AppDatabase appDb = AppDatabase.getAppDatabase(mContext); //MOVE TO CONTAINER OR SINGLETON
                appDb.daoInterfaceTranslation().delete(translation);
            }

            @Override
            public void imageViewOnClick(ImageView v, int position) {
                Logger.d("imageViewOnClick on position "+position);
                Translation translation = translationList.get(position);
                if(translation.getFavorite()) {
                    translation.setFavorite(false);
                    CommonHelper.colorizeImageView(v, R.color.un_favourite, mContext);
                }
                else {
                    translation.setFavorite(true);
                    CommonHelper.colorizeImageView(v, R.color.main_blue, mContext);
                }

                AppDatabase appDb = AppDatabase.getAppDatabase(mContext); //MOVE TO CONTAINER OR SINGLETON


                appDb.daoInterfaceTranslation().update(translation);

            }
        };

        mAdapter = new SavedTranslationAdapter(translationList, listener, mContext);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL)); //SET VERTICAL DIVIDER
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);

        //TODO: MOVE TO HELPER
        //COLORIZE ICON START
        Drawable drawable = menu.findItem(R.id.action_exercise).getIcon();
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, ContextCompat.getColor(this,R.color.white));
        menu.findItem(R.id.action_exercise).setIcon(drawable);
        //COLORIZE ICON END


        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        if(searchManager != null) {
            searchView = (SearchView) menu.findItem(R.id.action_search)
                    .getActionView();
            searchView.setSearchableInfo(searchManager
                    .getSearchableInfo(getComponentName()));
            searchView.setMaxWidth(Integer.MAX_VALUE);

            // listening to search query text change
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    // filter recycler view when query submitted
                    Logger.d("Query is : " + query);
                    mAdapter.getFilter().filter(query);
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String query) {
                    // filter recycler view when text is changed
                    Logger.d("onQueryTextChange is : " + query);
                    mAdapter.getFilter().filter(query);
                    return false;
                }
            });
        }
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_exercise) {
            Logger.d("Exercise");
            if(Translation.getInstance().getCount() > 0)
                CommonHelper.startActivity(ExerciseActivity.class);
            else
                ToastHelper.showToastMessage(this, R.string.no_saved_phrases, true);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void udpateDataInView() {
        fillAdapter();
    }

}

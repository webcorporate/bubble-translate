package phonetoaster.com.bubbletranslate.common;

import phonetoaster.com.bubbletranslate.BuildConfig;

public class Constants {

    public static class Global {
        public static final Boolean DEBUG = BuildConfig.DEBUG;
        public static final String PACKAGE_NAME = "phonetoaster.com.bubbletranslate";
        public static final boolean FLING_ANIMATION_ENABLED = false;
        public static final String APP_VERSION = "1.0"; //TODO GET VERSION FROM GRADLE CONFIG
        public static final String EXCEPTION = "exception";
        public static final String CONTENT_TYPE = "Content-Type8";
        public static final String TRANSLATE_TEXT_ENCODING = "UTF-8";
        //public static final String TRANSLATE_URL_ENDPOINT = "https://translate.googleapis.com/translate_a/single?client=gtx&sl=CS&ie=UTF-8&oe=UTF-8&tl=EN&dt=t";
        public static final String TRANSLATE_URL_ENDPOINT = "https://translate.googleapis.com/translate_a/single?client=gtx";
        public static final String URL_KEY_RESPONSE_TYPE = "&dt=t";
        public static final String URL_KEY_SOURCE_LANGUAGE = "&sl=";
        public static final String URL_KEY_TRANSLATION_LANGUAGE = "&tl=";
        public static final String URL_KEY_QUERY = "&q=";
        public static final String URL_KEY_INPUT_TEXT_ENCODING = "&ie=";
        public static final String URL_KEY_OUTPUT_TEXT_ENCODING = "&oe=";
        public static final String PREFERENCES_APP_ENABLED = "preferences_app_enabled";
        public static final String ADDITIONAL_PERMISSIONS_ALLOWED = "additional_permissions_allowed";
        public static final String PREFERENCES_TRANSLATION_TYPE = "preferences_translation_type";
        public static final String PREFERENCES_TRANSLATION_SOURCE_LANG = "preferences_translation_source_lang";
        public static final String PREFERENCES_TRANSLATION_DEST_LANG = "preferences_translation_dest_lang";
        public static final String PREFERENCES_BUBBLE_POSITION = "preferences_bubble_default_position";
        public static final String MOVE_LEFT = "left";
        public static final String MOVE_RIGHT = "right";
        public static final double VISIBILITY_TRESHOLD = 0.7;
        public static final int ANIM_DURATION = 1000;
        public static final int BUBBLE_DISMISS_DURATION = 15000;
        public static final int CONNECTION_TIMEOUT = 5000;
        public static final int READ_TIMEOUT = 1000;
        public static final String SOURCE_TEXT = "source_text";
        public static final String TRANSLATION = "translation";
        public static final String OPERATION_TYPE = "operation_type";
        public static final String OPERATION_TYPE_COPY = "operation_type_copy";
        public static final String OPERATION_TYPE_SAVE = "operation_type_save";
        public static final String X_COORDS = "x_coords";
        public static final String Y_COORDS = "y_coords";
        public static final String TRANSLATION_MODE = "translation_mode";
        public static final String SERVICE_DEFAULT_START = "default_init";
        public static final String SWIPE_DIRECTION_RIGHT = "right";
        public static final String SWIPE_DIRECTION_LEFT = "left";
        public static final String PACKAGE_PROTOCOL = "package:";
        public static final int MOVING_Y_AXIS_TRESHOLD = 100;
        public static final String LANG_SOURCE = "source";
        public static final String LANG_TRANSLATION = "translation";
        public static final int TRANSITION_ROTATE_ANGLE = 180;
        public static final int ANIMATION_DURATION = 500;
        public static final String MOVE_TOP_BOTTOM = "move_top_bottom";
        public static final String MOVE_BOTTOM_TOP = "move_bottom_top";
        public static final int BUBBLE_DEF_POSITION_SIDE_PADDING_PERCENT = 10;
        public static final int BUBBLE_DEF_POSITION_PADDING_BOTTOM_LEFT = 150;
        public static final int BUBBLE_DEF_POSITION_PADDING_BOTTOM_RIGHT = 500;
        public static final String SCREEN_WIDTH = "screen_width";
        public static final String SCREEN_HEIGHT = "screen_height";
        public static final String DATABASE_NAME = "translato-database";
        public static final String KEY_SHOULD_RELOAD_TRANSLATION = "should_reload_translation";
    }

    public class Answers {
        public static final int GREAT = 1;
        public static final int GOOD = 2;
        public static final int WRONG = 3;
    }

    public class TranslationMode {
        public static final String NOTIFICATION = "notification";
        public static final String FLOATING_WINDOW = "floating_window";
        public static final String TOAST_MESSAGE = "toast_message";
    }

    public class ExceptionMessage {
        public static final String EXC_CANNOT_CANNOT_PROCESS_REBOOT_RECEIVER = "Cannot process reboot receiver";
    }

    public static class Permission {
        public static final int SYSTEM_ALERT_WINDOW_CODE = 101;
        public static final int MANAGE_OVERLAY_PERMISSION_CODE = 112;
    }

    public static class Broadcast{
        public static final String GLOBAL_UPDATE_EVENT = "GLOBAL_UPDATE_EVENT";
    }

    public static class SharedPrefs {
        public static final String NAMESPACE = "translato";
        public static final String KEY_IS_FIRST_START = "is_first_start";
        public static final String KEY_APP_VERSION = "app_version";
    }
}
package phonetoaster.com.bubbletranslate.worker;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;


import androidx.work.Worker;
import androidx.work.WorkerParameters;
import phonetoaster.com.bubbletranslate.helper.CommonHelper;
import phonetoaster.com.bubbletranslate.service.TranslateService;


public class TranslateWorker extends Worker {

    private Context context;

    public TranslateWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        this.context = context;
    }

    @NonNull
    @Override
    public Result doWork() {
        Intent i = new Intent(context, TranslateService.class);
        CommonHelper.runService(context, i);
        return Worker.Result.success();
    }
}

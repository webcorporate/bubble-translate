package phonetoaster.com.bubbletranslate.listener;

import android.view.View;

/**
 * Created by george on 2/15/18.
 */

public interface RecyclerViewClickListener {

    void onClick(View view, int position);
}

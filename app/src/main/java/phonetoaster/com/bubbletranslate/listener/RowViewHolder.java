package phonetoaster.com.bubbletranslate.listener;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import phonetoaster.com.bubbletranslate.R;

/**
 * Created by george on 2/15/18.
 */

public class RowViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private RecyclerViewClickListener mListener;
    public TextView sourceTv;

    public RowViewHolder(View v, RecyclerViewClickListener listener) {
        super(v);
        sourceTv = (TextView) v.findViewById(R.id.sourceTv);
        mListener = listener;
        v.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        mListener.onClick(view, getAdapterPosition());
    }
}

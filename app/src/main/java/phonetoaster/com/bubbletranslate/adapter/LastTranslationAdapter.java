package phonetoaster.com.bubbletranslate.adapter;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.List;

import phonetoaster.com.bubbletranslate.R;
import phonetoaster.com.bubbletranslate.helper.CommonHelper;
import phonetoaster.com.bubbletranslate.model.LastTranslation;

/**
 * Created by george on 2/15/18.
 */

public class LastTranslationAdapter extends RecyclerSwipeAdapter<LastTranslationAdapter.MyViewHolder>  {

    private List<LastTranslation> mDataset = new ArrayList<>();
    private Context mContext;
    public static OnItemClickListener listener;

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.last_translation_swipe_layout;
    }

    /*Interface*/
    public interface OnItemClickListener {
        void onListItemSelected(LastTranslation contact);
        void imageViewOnClick(ImageView v, int position);
        void onListItemDeleted(LastTranslation contact);
    }

    public LastTranslationAdapter(List<LastTranslation> list, OnItemClickListener listener, Context context) {
        this.listener = listener;
        this.mDataset = list;
        this.mContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_translation, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        LastTranslation translation = mDataset.get(position);
        holder.sourceTv.setText(translation.getSourceText());
        holder.translationTv.setText(translation.getTranslatedText());



        //holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);
        holder.swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {
                YoYo.with(Techniques.Tada).duration(500).delay(100).playOn(layout.findViewById(R.id.translationRemoveIv));
                Logger.d("OnOpen on position "+holder.getAdapterPosition());
            }
        });

        holder.saveItemIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Logger.d("onClick");
                listener.imageViewOnClick(holder.saveItemIv, holder.getAdapterPosition());
            }
        });

        holder.deleteItemIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Logger.d("onClick deleteItemIv");
                int adapterPosition = holder.getAdapterPosition();
                LastTranslation translationToDelete = mDataset.get(adapterPosition);
                mDataset.remove(adapterPosition);
                notifyItemRemoved(adapterPosition);
                notifyItemRangeChanged(adapterPosition, mDataset.size());
                listener.onListItemDeleted(translationToDelete);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView saveItemIv, deleteItemIv;
        TextView sourceTv, translationTv;
        SwipeLayout swipeLayout;

        public MyViewHolder(final View view) {
            super(view);
            sourceTv = (TextView) view.findViewById(R.id.sourceTv);
            translationTv = (TextView) view.findViewById(R.id.translationTv);
            deleteItemIv = (ImageView) view.findViewById(R.id.translationRemoveIv);
            saveItemIv = (ImageView) view.findViewById(R.id.save_Item_Iv);
            swipeLayout = (SwipeLayout)view.findViewById(R.id.sample1);
        }
    }
}

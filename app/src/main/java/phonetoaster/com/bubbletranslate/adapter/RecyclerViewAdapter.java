package phonetoaster.com.bubbletranslate.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import phonetoaster.com.bubbletranslate.R;
import phonetoaster.com.bubbletranslate.listener.RecyclerViewClickListener;
import phonetoaster.com.bubbletranslate.listener.RowViewHolder;
import phonetoaster.com.bubbletranslate.model.Translation;

/**
 * Created by george on 2/15/18.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private RecyclerViewClickListener mListener;
    private List<Translation> mDataset = new ArrayList<>();
    private List<Translation> mDatasetFiltered = new ArrayList<>();
    public RecyclerViewAdapter(RecyclerViewClickListener listener) {
        mListener = listener;
    }

    public void updateData(List<Translation> dataset) {
        mDataset.clear();
        mDataset.addAll(dataset);
        mDatasetFiltered.addAll(dataset);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View v = LayoutInflater.from(context).inflate(R.layout.list_item_translation, parent, false);
        return new RowViewHolder(v, mListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof RowViewHolder) {
            Translation movie = mDataset.get(position);
            RowViewHolder rowHolder = (RowViewHolder) holder;
            rowHolder.sourceTv.setText(movie.getSourceText());
            //set values of data here
        }
    }

    @Override
    public int getItemCount() {
        return mDatasetFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                Logger.d(charString);
                if (charString.isEmpty()) {
                    mDatasetFiltered = mDataset;
                } else {
                    List<Translation> filteredList = new ArrayList<>();
                    for (Translation row : mDataset) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getSourceText().toLowerCase(Locale.getDefault()).contains(charString.toLowerCase()) ||
                            row.getSourceText().toLowerCase(Locale.getDefault()).startsWith(charString.toLowerCase())
                            ) {
                            filteredList.add(row);
                        }
                    }

                    mDatasetFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mDatasetFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mDatasetFiltered = (ArrayList<Translation>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}

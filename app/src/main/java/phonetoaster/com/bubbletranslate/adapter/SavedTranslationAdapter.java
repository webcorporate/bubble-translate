package phonetoaster.com.bubbletranslate.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.BaseSwipeAdapter;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;



import phonetoaster.com.bubbletranslate.R;
import phonetoaster.com.bubbletranslate.helper.CommonHelper;
import phonetoaster.com.bubbletranslate.model.Translation;

/**
 * Created by george on 2/15/18.
 */

public class SavedTranslationAdapter extends RecyclerSwipeAdapter<SavedTranslationAdapter.MyViewHolder> implements Filterable {

    private List<Translation> mDataset = new ArrayList<>();
    private List<Translation> mDatasetFiltered = new ArrayList<>();
    private Context mContext;
    public static OnItemClickListener listener;

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.sample1;
    }

    /*Interface*/
    public interface OnItemClickListener {
        void onListItemSelected(Translation contact);
        void imageViewOnClick(ImageView v, int position);
        void onListItemDeleted(Translation contact);
    }

    public SavedTranslationAdapter(List<Translation> contactList, OnItemClickListener listener, Context context) {
        this.listener = listener;
        this.mDataset = contactList;
        this.mDatasetFiltered = contactList;
        this.mContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_translation, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Translation translation = mDatasetFiltered.get(position);
        holder.sourceTv.setText(translation.getSourceText());
        holder.translationTv.setText(translation.getTranslatedText());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            holder.starIv.setImageDrawable(mContext.getDrawable(R.drawable.star));
        }
        if(translation.getFavorite())
            CommonHelper.colorizeImageView(holder.starIv, R.color.main_blue, mContext);
        else
            CommonHelper.colorizeImageView(holder.starIv, R.color.un_favourite, mContext);

        //holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);
        holder.swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {
                YoYo.with(Techniques.Tada).duration(500).delay(100).playOn(layout.findViewById(R.id.translationRemoveIv));
                Logger.d("OnOpen on position "+holder.getAdapterPosition());
            }
        });

        holder.starIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Logger.d("onClick");
                listener.imageViewOnClick(holder.starIv, holder.getAdapterPosition());
            }
        });

        holder.deleteItemIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Logger.d("onClick deleteItemIv");
                int adapterPosition = holder.getAdapterPosition();
                Translation translationToDelete = mDatasetFiltered.get(adapterPosition);
                mDatasetFiltered.remove(adapterPosition);
                notifyItemRemoved(adapterPosition);
                notifyItemRangeChanged(adapterPosition, mDatasetFiltered.size());
                listener.onListItemDeleted(translationToDelete);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mDatasetFiltered.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView starIv, deleteItemIv;
        TextView sourceTv, translationTv;
        SwipeLayout swipeLayout;

        public MyViewHolder(final View view) {
            super(view);
            sourceTv = (TextView) view.findViewById(R.id.sourceTv);
            translationTv = (TextView) view.findViewById(R.id.translationTv);
            deleteItemIv = (ImageView) view.findViewById(R.id.translationRemoveIv);
            starIv = (ImageView) view.findViewById(R.id.save_Item_Iv);
            swipeLayout = (SwipeLayout)view.findViewById(R.id.sample1);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mDatasetFiltered = mDataset;
                } else {
                    List<Translation> filteredList = new ArrayList<>();
                    for (Translation row : mDataset) {
                        //TODO: REMOVE DIACRITICS BEFORE FILTERING
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                if (row.getTranslatedText().toLowerCase(Locale.getDefault()).contains(charString.toLowerCase()) ||
                    row.getTranslatedText().toLowerCase(Locale.getDefault()).contains(charString.toLowerCase()) ||
                    row.getSourceText().toLowerCase(Locale.getDefault()).contains(charString.toLowerCase()) ||
                    row.getSourceText().toLowerCase(Locale.getDefault()).startsWith(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }
                    mDatasetFiltered = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mDatasetFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mDatasetFiltered = (ArrayList<Translation>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}

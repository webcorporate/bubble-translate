package phonetoaster.com.bubbletranslate.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.orhanobut.logger.Logger;

import java.util.List;

import phonetoaster.com.bubbletranslate.R;
import phonetoaster.com.bubbletranslate.activity.DashBoardActivity;
import phonetoaster.com.bubbletranslate.activity.ExerciseActivity;
import phonetoaster.com.bubbletranslate.activity.SavedTranslationsActivity;
import phonetoaster.com.bubbletranslate.container.Container;
import phonetoaster.com.bubbletranslate.helper.AppDatabase;
import phonetoaster.com.bubbletranslate.helper.CommonHelper;
import phonetoaster.com.bubbletranslate.model.Translation;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ExerciseResultSlidePageFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ExerciseResultSlidePageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ExerciseResultSlidePageFragment extends Fragment {

    private static Integer mPosition;
    private static Integer mListSize;
    private Button mButtonExerciseAgain, mButtonExerciseEnd;
    private TextView mTotalCountOfWordsTv, mTextViewQuestTotalCount, mOveralScoreTv, mOveralScoreDescriptionTv;
    private AppDatabase mAppDb;
    private List<Translation> mTranslationList;
    private ExerciseActivity ea;
    private static final String ACTUAL_ITEM_NO = "actual_item_no";
    private static final String LIST_SIZE = "list_size";


    public static ExerciseResultSlidePageFragment newInstance(Integer position, Integer size) {
        ExerciseResultSlidePageFragment fragment = new ExerciseResultSlidePageFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ACTUAL_ITEM_NO, position+1);
        bundle.putInt(LIST_SIZE, size);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //////////////////////////////
        //INIT COMMON
        //////////////////////////////
        initCommon();
        //////////////////////////////
        //INIT VIEWS
        //////////////////////////////
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.result_item, container, false);
        initViews(rootView);

        //////////////////////////////
        //INIT LISTENERS
        //////////////////////////////
        initListeners();
        disableCardMoving();

        //////////////////////////////
        //SET VALUES
        //////////////////////////////
        setValues();

        return rootView;
    }


    private void initCommon() {
        ea = ((ExerciseActivity) getActivity());
        mPosition = getArguments().getInt(ACTUAL_ITEM_NO,0);
        mListSize = getArguments().getInt(LIST_SIZE,0);
        mAppDb = AppDatabase.getAppDatabase(getContext()); //TODO: MOVE TO SINGLETON
        mTranslationList = getTranslations();
    }

    private void initViews(ViewGroup rootView) {
        mButtonExerciseAgain = (Button) rootView.findViewById(R.id.exercise_again_btn);
        mButtonExerciseEnd = (Button) rootView.findViewById(R.id.exercise_end_btn);
        mTotalCountOfWordsTv = (TextView) rootView.findViewById(R.id.total_count_of_words_tv);
        mOveralScoreTv = (TextView) rootView.findViewById(R.id.result_excersise_score_tv);
        mOveralScoreDescriptionTv = (TextView) rootView.findViewById(R.id.result_overal_score_description_tv);
    }

    private void initListeners() {
        mButtonExerciseAgain.setOnClickListener(mButtonExerciseAgainListener);
        mButtonExerciseEnd.setOnClickListener(mButtonExerciseEndListener);
    }

    private void setValues() {
        mTotalCountOfWordsTv.setText(this.getString(R.string.x_phrases_exercised, String.valueOf(mTranslationList.size())));
        mOveralScoreTv.setText(this.getString(R.string.x_exercise_overal_score, CommonHelper.trimDouble(getOverallScore())));
        //mOveralScoreDescriptionTv.setText(this.getString(R.string.x_exercise_overal_score, getOverallScore().toString()));
    }

    private View.OnClickListener mButtonExerciseAgainListener = new View.OnClickListener() {
        public void onClick(View v) {
            Logger.d("mButtonExerciseAgainListener");
            if(ea != null) {
                ea.moveToFirstSlide();
                ea.setSessionId();
            }
        }
    };

    private View.OnClickListener mButtonExerciseEndListener = new View.OnClickListener() {
        public void onClick(View v) {
            Logger.d("mButtonExerciseEndListener");
            Container.getInstance().resetExerciseSessionId();
            CommonHelper.startActivity(DashBoardActivity.class);
        }
    };

    private void disableCardMoving() {
        if(ea != null)
            ea.disableSlide();
    }

    private List<Translation> getTranslations() {
        return  mAppDb.daoInterfaceTranslation().getAll();
    }

    private Double getOverallScore() {
        return  mAppDb.daoInterfaceExercise().avgOfGivenSession(Container.getInstance().getmExerciseSessionId());
    }
}

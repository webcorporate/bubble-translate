package phonetoaster.com.bubbletranslate.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.orhanobut.logger.Logger;

import phonetoaster.com.bubbletranslate.R;
import phonetoaster.com.bubbletranslate.activity.ExerciseActivity;
import phonetoaster.com.bubbletranslate.common.Constants;
import phonetoaster.com.bubbletranslate.container.Container;
import phonetoaster.com.bubbletranslate.helper.CommonHelper;
import phonetoaster.com.bubbletranslate.model.Exercise;
import phonetoaster.com.bubbletranslate.model.Translation;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ScreenSlidePageFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ScreenSlidePageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ScreenSlidePageFragment extends Fragment {

    private static Integer mPosition;
    private static Integer mListSize;
    private TextView mSourceTextTv;
    private TextView mTranslationTextTv;
    private TextView mPositionTv;
    private TextView mProgressTv;
    private TextView mWordAvgScoreTv;
    private LinearLayout mRateAnswerSectionWraper;
    private Button mButtonShowAnswer ,mButtonAnswerGreat, mButtonAnswerGood, mButtonAnswerWrong;

    private static final String DESCRIBABLE_KEY = "describable_key";
    private static final String ACTUAL_ITEM_NO = "actual_item_no";
    private static final String LIST_SIZE = "list_size";
    private Translation mTranslation;


    public static ScreenSlidePageFragment newInstance(Translation describable, Integer position, Integer size) {
        ScreenSlidePageFragment fragment = new ScreenSlidePageFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(DESCRIBABLE_KEY, describable);
        bundle.putInt(ACTUAL_ITEM_NO, position+1);
        bundle.putInt(LIST_SIZE, size);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //////////////////////////////
        //INIT VIEWS
        //////////////////////////////
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.card_item, container, false);
        initViews(rootView);

        //////////////////////////////
        //INIT LISTENERS
        //////////////////////////////
        initListeners();

        //////////////////////////////
        //GET VALUES FROM BUNDLE
        //////////////////////////////
        mTranslation = (Translation) getArguments().getSerializable(
                DESCRIBABLE_KEY);

        mPosition = getArguments().getInt(ACTUAL_ITEM_NO,0);
        mListSize = getArguments().getInt(LIST_SIZE,0);

        //////////////////////////////
        //SET VALUES FORM BUNDLE TO VIEW
        //////////////////////////////
        mProgressTv.setText(getString(R.string.question_progress, mPosition.toString(), mListSize.toString()));
        mWordAvgScoreTv.setText(getAvgScoreForCard());
        mSourceTextTv.setText(mTranslation.getSourceText());
        mTranslationTextTv.setText(mTranslation.getTranslatedText());



        return rootView;
    }


    private void initViews(ViewGroup rootView) {
        mProgressTv = (TextView) rootView.findViewById(R.id.progressTv);
        mSourceTextTv = (TextView) rootView.findViewById(R.id.source_text);
        mTranslationTextTv = (TextView) rootView.findViewById(R.id.translated_text);
        mWordAvgScoreTv = (TextView) rootView.findViewById(R.id.wordAvgScoreTv);
        mButtonShowAnswer = (Button) rootView.findViewById(R.id.show_answer_btn);
        mButtonAnswerGreat = (Button) rootView.findViewById(R.id.group_button_great);
        mButtonAnswerGood = (Button) rootView.findViewById(R.id.group_button_good);
        mButtonAnswerWrong = (Button) rootView.findViewById(R.id.group_button_wrong);
        mRateAnswerSectionWraper = rootView.findViewById(R.id.rate_section_wrapper);
        mTranslationTextTv.setVisibility(View.INVISIBLE);
        disableAllButtons();
    }

    private void initListeners() {
        mButtonShowAnswer.setOnClickListener(mButtonShowAnswerListener);
        mButtonAnswerGreat.setOnClickListener(mButtonSwitchAnswerListener);
        mButtonAnswerGood.setOnClickListener(mButtonSwitchAnswerListener);
        mButtonAnswerWrong.setOnClickListener(mButtonSwitchAnswerListener);
    }

    private View.OnClickListener mButtonShowAnswerListener = new View.OnClickListener() {

        public void onClick(View v) {
            Logger.d("mButtonShowAnswerListener");
            Animation slideUp = AnimationUtils.loadAnimation(getContext(), R.anim.slide_up);
            mTranslationTextTv.setVisibility(View.VISIBLE);
            mRateAnswerSectionWraper.setVisibility(View.VISIBLE);
            mRateAnswerSectionWraper.startAnimation(slideUp);
            enableAllButtons();
        }
    };

    private View.OnClickListener mButtonSwitchAnswerListener = new View.OnClickListener() {

        public void onClick(View v) {
            Button[] buttons = new Button[]{mButtonAnswerGreat, mButtonAnswerGood, mButtonAnswerWrong};
            for (Button b: buttons   ) {
                if(b.equals(v)) // the clicked one
                {
                    b.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    b.setTextColor(getResources().getColor(R.color.white));
                    Integer buttonId = v.getId() ;
                    if (buttonId == R.id.group_button_great) {
                        saveAnswer(Constants.Answers.GREAT);
                    }
                    if (buttonId == R.id.group_button_good) {
                        saveAnswer(Constants.Answers.GOOD);
                    }
                    if (buttonId == R.id.group_button_wrong) {
                        saveAnswer(Constants.Answers.WRONG);
                    }
                }
                else  // the others
                {
                    b.setBackgroundColor(getResources().getColor(R.color.great));
                    b.setTextColor(getResources().getColor(R.color.white));
                }
            }
        }
    };

    private String getAvgScoreForCard() {
        Double averageScore = Exercise.getInstance().getAverageScoreForCard(mTranslation.getId());
        return this.getString(R.string.avg_score, CommonHelper.trimDouble(averageScore));
    }

    private void saveAnswer(Integer rating) {
        Logger.d(rating);
        Integer lastExerciseSessionId = Container.getInstance().getmExerciseSessionId();
        Exercise exercise = new Exercise(mTranslation.getId(), rating, lastExerciseSessionId);
        exercise.save();
        getAvgScoreForCard();
        moveToNextCard();
    }

    private void moveToNextCard() {
        final Handler handler = new Handler();
        final ExerciseActivity ea = ((ExerciseActivity) getActivity());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(ea != null) {
                    ea.moveToNextSlide();
                }
            }
        }, 200); //RUN AFTER DELAY
    }

    private void disableAllButtons() {
        mButtonAnswerGreat.setEnabled(false);
        mButtonAnswerGood.setEnabled(false);
        mButtonAnswerWrong.setEnabled(false);
    }

    private void enableAllButtons() {
        mButtonAnswerGreat.setEnabled(true);
        mButtonAnswerGood.setEnabled(true);
        mButtonAnswerWrong.setEnabled(true);
    }
}

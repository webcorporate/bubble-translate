package phonetoaster.com.bubbletranslate.helper;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;

import com.orhanobut.logger.Logger;

import phonetoaster.com.bubbletranslate.R;
import phonetoaster.com.bubbletranslate.common.Constants;
import phonetoaster.com.bubbletranslate.receiver.NotificationReceiver;

import static android.content.Context.NOTIFICATION_SERVICE;

public class NotificationHelper {

    public static String ANDROID_CHANNEL_ID = Constants.Global.PACKAGE_NAME.concat("DEFAULT"); //TODO: MOVE TO CONSTANTS
    public static String ANDROID_CHANNEL_ID_SYNC = "sync_channel";//TODO: MOVE TO CONSTANTS
    public static Integer SERVICE_NOTIFICATION_ID = 1;//TODO: MOVE TO CONSTANTS
    private static final String ANDROID_CHANNEL_NAME = "Default channel";
    private static final String ANDROID_CHANNEL_SYNC = "Sync channel";

    public static void showNotification(String sourceText, String translatedText, Context context) {
        Logger.d("showNotification "+sourceText+" "+translatedText);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
        mBuilder.setSmallIcon(R.drawable.ic_info_black_24dp)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                .setContentTitle(context.getString(R.string.your_translation)+ " "+context.getString(R.string.of)+ ": "+sourceText)
                .setContentText(translatedText)
                .addAction(R.drawable.md_btn_selector_dark,
                        context.getString(R.string.copy_to_clipboard),
                        makePendingIntentCopy(context, translatedText))
                .addAction(R.drawable.md_btn_selector_dark,
                    context.getString(R.string.save),
                        pendingIntentSaveTranslation(context, sourceText, translatedText));

        // Sets an ID for the notification
        int mNotificationId = 001;
        // Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr =
                (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        // Builds the notification and issues it.
        if(mNotifyMgr != null)
            mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }

    public static void addServiceNotification(Service context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                setupNotificationChannelsLowImportance(notificationManager);
            }
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(context, ANDROID_CHANNEL_ID_SYNC);
            Notification notification = mBuilder
                    .setOngoing(false) //Always true in start foreground
                    .setAutoCancel(true)
                    .setSmallIcon(R.drawable.ic_sync)
                    .setContentTitle(getSyncingMessage(context))
                    .setPriority(NotificationManager.IMPORTANCE_LOW)
                    .setVisibility(Notification.VISIBILITY_PRIVATE)
                    .setCategory(Notification.CATEGORY_SERVICE)
                    .build();
            notification.flags |=Notification.FLAG_AUTO_CANCEL;
            context.startForeground(SERVICE_NOTIFICATION_ID, notification);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static void setupNotificationChannelsLowImportance(NotificationManager  notificationManager){
        Logger.i("setupNotificationChannelsLowImportance");
        try {
            NotificationChannel androidChannel = new NotificationChannel(ANDROID_CHANNEL_ID_SYNC, ANDROID_CHANNEL_SYNC, NotificationManager.IMPORTANCE_LOW);
            androidChannel.enableLights(false);
            androidChannel.enableVibration(false);
            androidChannel.setImportance(NotificationManager.IMPORTANCE_LOW);
            notificationManager.createNotificationChannel(androidChannel);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String getSyncingMessage(Context context) {
        String message = "";
        if (Constants.Global.DEBUG) {
            message = context.getString(R.string.processing)+" "+context.getClass().getSimpleName();
        } else {
            message = context.getString(R.string.processing_translation);
        }
        return message;
    }

    private static PendingIntent makePendingIntentCopy(Context context, String translatedText) {
        Intent intentAction = new Intent(context, NotificationReceiver.class);
        intentAction.putExtra(Constants.Global.OPERATION_TYPE, Constants.Global.OPERATION_TYPE_COPY);
        intentAction.putExtra(Constants.Global.TRANSLATION, translatedText);
        return PendingIntent.getBroadcast(context, 1, intentAction, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private static PendingIntent pendingIntentSaveTranslation(Context context,String sourceText, String translatedText) {
        Intent intentAction = new Intent(context, NotificationReceiver.class);
        intentAction.putExtra(Constants.Global.OPERATION_TYPE, Constants.Global.OPERATION_TYPE_SAVE);
        intentAction.putExtra(Constants.Global.SOURCE_TEXT, sourceText);
        intentAction.putExtra(Constants.Global.TRANSLATION, translatedText);
        return PendingIntent.getBroadcast(context, 2, intentAction, PendingIntent.FLAG_UPDATE_CURRENT);
    }

}

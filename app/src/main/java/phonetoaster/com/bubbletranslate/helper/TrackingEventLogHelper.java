package phonetoaster.com.bubbletranslate.helper;

import android.app.Application;
import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.StandardExceptionParser;
import com.google.android.gms.analytics.Tracker;
import com.orhanobut.logger.Logger;

public class TrackingEventLogHelper extends Application {
    private static Context trackerContext;

    public static synchronized Tracker getInstance(Context context) {
        if (trackerContext == null) {
            GoogleAnalyticsHelper.initialize(context);
            trackerContext = context;
        }
        return GoogleAnalyticsHelper.getInstance().get(GoogleAnalyticsHelper.Target.APP);
    }

    public static void logException(Exception e, String customTag, String customMessage, Boolean isFatal) {
        if (e != null) {
            try {
                Tracker t = getInstance(trackerContext);
                t.send(new HitBuilders.ExceptionBuilder()
                                .setDescription(
                                        new StandardExceptionParser(trackerContext, null)
                                                .getDescription(Thread.currentThread().getName(), e))
                                .setFatal(isFatal)
                                .set(customTag, customMessage)
                                .build()
                );
                Logger.e(customMessage);
                Logger.e(e.getMessage());
                Logger.e("Exception log send");
            } catch (Exception exception) {
                Logger.e(exception.getMessage());
                exception.printStackTrace();
            }

        }
    }

    public static void logEvent(String category, String action, String label) {
        Tracker t = getInstance(trackerContext);
        // Build and send an Event.
        t.send(new HitBuilders.EventBuilder()
                .setCategory(category)
                .setAction(action)
                .setLabel(label)
                .build());
    }

    public static void logScreenView(String screenName) {
        Tracker t = getInstance(trackerContext);
        // Set screen name.
        t.setScreenName(screenName);
        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());
        GoogleAnalytics.getInstance(trackerContext).dispatchLocalHits();
    }


}

package phonetoaster.com.bubbletranslate.helper;

import android.content.Context;
import android.content.SharedPreferences;
import phonetoaster.com.bubbletranslate.common.Constants;


/**
 * healthHelm
 * File Created by vavru on 7.9.2015.
 * Copyright (c) 2015 CertiCon a.s. All rights reserved.
 */
public class SharedPrefsHelper {
    private SharedPreferences sharedPreferences;
    private static SharedPrefsHelper instance;

    private SharedPrefsHelper(Context ctx) {
        sharedPreferences = ctx.getSharedPreferences(Constants.SharedPrefs.NAMESPACE, Context.MODE_PRIVATE);
    }

    /**
     * Gets the instance of SharedPrefHelper (singleton)
     *
     * @param context
     * @return instance of the SharedPrefsHelper class
     */
    public static synchronized SharedPrefsHelper getInstance(Context context) {
        if (instance == null) {
            instance = new SharedPrefsHelper(context);
        }
        return instance;
    }

    /**
     * Creates and put string into the shared preferences file
     *
     * @param key   unique key under which would be the string saved
     * @param value string to be saved
     */
    public void createBoolean(String key, Boolean value) {
        sharedPreferences
                .edit()
                .putBoolean(key, value)
                .apply();
    }

    /**
     * Reads string from the shared preferences matched by the given key
     *
     * @param key key representing the saved string
     * @return saved string
     */
    public Boolean readBoolean(String key) {
        return sharedPreferences.getBoolean(key, false);
    }

    /**
     * Updates string in the shared preferences file under the given key
     *
     * @param key   key representing the saved string
     * @param value new string to be saved
     */
    public void updateBoolean(String key, Boolean value) {
        sharedPreferences
                .edit()
                .putBoolean(key, value)
                .apply();
    }

    /**
     * Creates and put string into the shared preferences file
     *
     * @param key   unique key under which would be the string saved
     * @param value string to be saved
     */
    public void createString(String key, String value) {
        sharedPreferences
                .edit()
                .putString(key, value)
                .apply();
    }

    /**
     * Reads string from the shared preferences matched by the given key
     *
     * @param key key representing the saved string
     * @return saved string
     */
    public String readString(String key) {
        return sharedPreferences.getString(key, null);
    }

    /**
     * Updates string in the shared preferences file under the given key
     *
     * @param key   key representing the saved string
     * @param value new string to be saved
     */
    public void updateString(String key, String value) {
        sharedPreferences
                .edit()
                .putString(key, value)
                .apply();
    }

    /**
     * Delete any record in the shared preferences file by the given key
     *
     * @param key key representing the saved value
     */
    public void delete(String key) {
        sharedPreferences
                .edit()
                .remove(key)
                .apply();
    }




}

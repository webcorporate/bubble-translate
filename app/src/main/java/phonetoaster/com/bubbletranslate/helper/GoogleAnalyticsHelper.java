package phonetoaster.com.bubbletranslate.helper;

import android.app.Application;
import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import java.util.HashMap;
import java.util.Map;

import phonetoaster.com.bubbletranslate.R;

public class GoogleAnalyticsHelper extends Application {

    private final Map<Target, Tracker> mTrackers = new HashMap<Target, Tracker>();
    private final Context mContext;
    private static GoogleAnalyticsHelper sInstance;

    public enum Target {
        APP,
        // Add more trackers here if you need, and update the code in #get(Target) below
    }

    /**
     * Don't instantiate directly - use {@link #getInstance()} instead.
     */
    private GoogleAnalyticsHelper(Context context) {
        mContext = context.getApplicationContext();
    }

    public static synchronized void initialize(Context context) {
        if (sInstance != null) {
            throw new IllegalStateException("Extra call to initialize analytics trackers");
        }
        sInstance = new GoogleAnalyticsHelper(context);
    }

    public static synchronized GoogleAnalyticsHelper getInstance() {
        if (sInstance == null) {
            throw new IllegalStateException("Call initialize() before getInstance()");
        }

        return sInstance;
    }


    public synchronized Tracker get(Target target) {
        if (!mTrackers.containsKey(target)) {
            Tracker tracker;
            switch (target) {
                case APP:
                    //TODO: CHANGE GA ID IN XML
                    tracker = GoogleAnalytics.getInstance(mContext).newTracker(R.xml.app_tracker);
                    break;
                default:
                    throw new IllegalArgumentException("Unhandled analytics target " + target);
            }
            mTrackers.put(target, tracker);
        }

        return mTrackers.get(target);
    }

}

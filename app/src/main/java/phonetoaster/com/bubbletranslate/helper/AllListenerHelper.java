package phonetoaster.com.bubbletranslate.helper;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.orhanobut.logger.Logger;

import phonetoaster.com.bubbletranslate.common.Constants;
import phonetoaster.com.bubbletranslate.service.ClipboardService;

public class AllListenerHelper {

    public static void initializeListeners(final Context context) {
        try {
            // Init Clipboard listener
            Intent myIntent = new Intent(context, ClipboardService.class);
            myIntent.putExtra(Constants.Global.SERVICE_DEFAULT_START, true);
            context.startService(myIntent);
        } catch (Exception e) {
            Logger.e(e.getMessage());
        }
    }

}

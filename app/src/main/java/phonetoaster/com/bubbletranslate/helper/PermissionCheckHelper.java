package phonetoaster.com.bubbletranslate.helper;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;

import com.orhanobut.logger.Logger;

import phonetoaster.com.bubbletranslate.service.ClipboardService;

public class PermissionCheckHelper {

    public static boolean isHigherOrEqualVersion23() {
        boolean isHigherOrEqualVersion23Bool = false;
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                isHigherOrEqualVersion23Bool = true;
            } else {
                isHigherOrEqualVersion23Bool = false;
            }
        } catch (Exception e) {
            Logger.e(e.getMessage());
        }
        return isHigherOrEqualVersion23Bool;
    }


    public static boolean hasPermissionToOverlayScreen(Context ctx) {
        boolean hasPermissionToOverlayScreen = false;
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (Settings.canDrawOverlays(ctx)) {
                    Logger.d("Permission was successfully granted, displaying again");
                    hasPermissionToOverlayScreen = true;
                } else {
                    Logger.d("Permission was not granted");
                    hasPermissionToOverlayScreen = false;
                }
            }
        } catch (Exception e) {
            Logger.e(e.getMessage());
        }
        return hasPermissionToOverlayScreen;
    }

}

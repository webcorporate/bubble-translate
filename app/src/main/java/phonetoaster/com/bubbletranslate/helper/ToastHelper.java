package phonetoaster.com.bubbletranslate.helper;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import com.orhanobut.logger.Logger;

import phonetoaster.com.bubbletranslate.common.Constants;

public class ToastHelper {

    public static void showToastMessage(final Context ctx, final String message, boolean isDebugMessage) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(
                new Runnable()
                {
                    @Override
                    public void run()
                    {
                        Toast.makeText(ctx, message, Toast.LENGTH_LONG).show();
                    }
                }
        );

        if(Constants.Global.DEBUG && isDebugMessage) {
            Logger.d(message);
        }
    }

    public static void showToastMessage(final Context ctx, final int messageId, boolean isDebugMessage) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(
                new Runnable()
                {
                    @Override
                    public void run()
                    {
                        Toast.makeText(ctx, ctx.getString(messageId), Toast.LENGTH_LONG).show();
                    }
                }
        );

        if(Constants.Global.DEBUG && isDebugMessage) {
            Logger.d(ctx.getString(messageId));
        }
    }


}

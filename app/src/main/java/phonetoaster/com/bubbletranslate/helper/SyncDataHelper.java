package phonetoaster.com.bubbletranslate.helper;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import phonetoaster.com.bubbletranslate.common.Constants;

/**
 * healthHelm
 * File Created by vavru on 14.12.2015.
 * Copyright (c) 2015 CertiCon a.s. All rights reserved.
 */
public class SyncDataHelper {

    public static void broadcastGlobalUpdate(Context context) {
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setAction(Constants.Broadcast.GLOBAL_UPDATE_EVENT);
        context.sendBroadcast(intent);
    }

}

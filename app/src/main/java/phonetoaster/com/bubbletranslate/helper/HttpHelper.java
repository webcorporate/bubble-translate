package phonetoaster.com.bubbletranslate.helper;

import com.orhanobut.logger.Logger;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import phonetoaster.com.bubbletranslate.common.Constants;

public class HttpHelper {

    public void doRequest(String url, Callback callback) throws IOException {
        Logger.d("Trying to do remote request from URL: "+url);
        //BUILDER
        OkHttpClient.Builder b = new OkHttpClient.Builder();
        b.connectTimeout(Constants.Global.CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS);
        b.writeTimeout(Constants.Global.CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS);
        b.readTimeout(Constants.Global.CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS);
        b.retryOnConnectionFailure(false);

        //REQUEST
        Request request = new Request.Builder()
                .addHeader(Constants.Global.CONTENT_TYPE, "text/json; Charset="+Constants.Global.TRANSLATE_TEXT_ENCODING)
                .url(url)
                .build();

        //CLIENT
        OkHttpClient client = b.build();
        client.newCall(request).enqueue(callback);
    }

}

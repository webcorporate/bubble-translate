package phonetoaster.com.bubbletranslate.helper;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import phonetoaster.com.bubbletranslate.common.Constants;
import phonetoaster.com.bubbletranslate.model.Exercise;
import phonetoaster.com.bubbletranslate.model.LastTranslation;
import phonetoaster.com.bubbletranslate.model.Translation;

/**
 * Created by george on 2/13/18.
 */

@Database(entities = {Translation.class, Exercise.class, LastTranslation.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;
    public abstract LastTranslation.DaoInterface daoInterfaceLastTranslation();
    public abstract Translation.DaoInterface daoInterfaceTranslation();
    public abstract Exercise.DaoInterface daoInterfaceExercise();

    public static AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, Constants.Global.DATABASE_NAME)
                            // allow queries on the main thread.
                            // Don't do this on a real app! See PersistenceBasicSample for an example.
                            //.fallbackToDestructiveMigration() //RELATED TO https://stackoverflow.com/questions/45376492/room-attempt-to-re-open-an-already-closed-database
                            .allowMainThreadQueries()
                            .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}

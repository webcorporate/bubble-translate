package phonetoaster.com.bubbletranslate.helper;

import android.app.Activity;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.Utils;
import com.orhanobut.logger.Logger;

import java.text.DecimalFormat;
import java.util.List;

import phonetoaster.com.bubbletranslate.R;
import phonetoaster.com.bubbletranslate.activity.PreferenceActivity;
import phonetoaster.com.bubbletranslate.common.Constants;
import phonetoaster.com.bubbletranslate.model.Preference;
import pub.devrel.easypermissions.EasyPermissions;

public class CommonHelper {

    public static void initAllLibraries() {

    }

    public static boolean isDeviceOnline(Context ctx) {
        ConnectivityManager manager =
                (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;
        if (networkInfo != null && networkInfo.isConnected()) {
            // Network is present and connected
            isAvailable = true;
        }
        return isAvailable;
    }

    public static boolean shouldAskPermission(){
        return(Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    public static boolean hasRequiredPermission(Context context, String[] requiredPermissions) {
        return EasyPermissions.hasPermissions(context, requiredPermissions);
    }

    public static boolean canAppDrawOverlays(Context context) {
        return Settings.canDrawOverlays(context);
    }

    public static String getNotificationType(Context context) {
        Logger.d("getNotificationType");
        return Preference.getInstance(context).getTranslationType();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public static boolean isAdditionalPermissionsSet(PreferenceActivity pa) throws RuntimeException {
        Logger.d("isAdditionalPermissionsSet");
        if (Settings.canDrawOverlays(pa)) {
            return true;
        } else {
            return false;
        }
    }

    public static void toogleView(View view) {
        int isVissible = view.getVisibility();
        if(isVissible == View.VISIBLE) {
            view.animate().alpha(0.0f);
            view.animate().setDuration(Constants.Global.ANIM_DURATION);
            view.animate().start();
            view.setVisibility(View.GONE);
            view.clearFocus(); //TODO: HIDE VIRTUAL KEYBOARD
        }
        else {
            view.animate().alpha(1.0f);
            view.animate().setDuration(Constants.Global.ANIM_DURATION);
            view.animate().start();
            view.setVisibility(View.VISIBLE);
        }
    }

    public static void navigateToActivity(String activityName, Context context) {
        try {
            Class<?> c = Class.forName(context.getPackageName()+".activity."+ activityName);
            Intent intent = new Intent(context, c);
            context.startActivity(intent);
        } catch (ClassNotFoundException e) {
            ExceptionLogHelper.log(e);
        }
    }

    public static int getScreenSize(String type, Context context) {
        int screenSize = 0;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
        if (wm != null) {
            wm.getDefaultDisplay().getMetrics(displayMetrics);
            int height = displayMetrics.heightPixels;
            int width = displayMetrics.widthPixels;
            switch (type) {
                case Constants.Global.SCREEN_WIDTH:
                    screenSize = width;
                    break;
                case Constants.Global.SCREEN_HEIGHT:
                    screenSize = height;
                    break;
            }
        }


        /*
        WindowManager wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
        if (wm != null) {
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getRealSize(size);
            switch (type){
                case Constants.Global.SCREEN_WIDTH:
                    screenSize = size.x;
                    break;
                case Constants.Global.SCREEN_HEIGHT:
                    screenSize = size.y;
                    break;
            }
        }
        */
        return screenSize;
    }

    public static void colorizeImageView(ImageView imageView, int color, Context context) {
        imageView.setColorFilter(context.getResources().getColor(color));
    }

    public static void startActivity(Class cls) {
        ActivityUtils.startActivity(cls);
    }

    public static String trimDouble(Double resultToTrim) {
        return new DecimalFormat("#.##").format(resultToTrim);
    }

    public static Boolean isAutostartAffectedDevice(Context context) {
        Boolean isAfftected = false;
        try {
            String manufacturer = android.os.Build.MANUFACTURER;
            if ("xiaomi".equalsIgnoreCase(manufacturer)) {
                isAfftected = true;
            } else if ("oppo".equalsIgnoreCase(manufacturer)) {
                isAfftected = true;
            } else if ("vivo".equalsIgnoreCase(manufacturer)) {
                isAfftected = true;
            } else if ("Letv".equalsIgnoreCase(manufacturer)) {
                isAfftected = true;
            } else if ("Honor".equalsIgnoreCase(manufacturer)) {
                isAfftected = true;
            }

        } catch (Exception e) {
            Logger.e(e.getMessage());
        }
        return isAfftected;
    }

    public static void checkAutostartPermission(final Context context) {
        Boolean autostartDiplayed = SharedPrefsHelper.getInstance(context).readBoolean(Constants.SharedPrefs.KEY_IS_FIRST_START);
        if((autostartDiplayed == null || !autostartDiplayed) && isAutostartAffectedDevice(context)) {
            showAutostartDialog(context);
        }
    }

    public static void showAutostartDialog(final Context context) {
        new MaterialDialog.Builder(context)
                .title(R.string.please_allow_autostart_of_application_to_receive_notification_after_restart)
                .content(R.string.to_receive_notification_after_restart)
                .positiveText(R.string.ok)
                .negativeText(R.string.cancel)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        navigateAutostartPermission(context);
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        SharedPrefsHelper.getInstance(context).createBoolean(Constants.SharedPrefs.KEY_IS_FIRST_START, true);
                    }
                })
                .show();
    }

    public static void navigateAutostartPermission(Context context) {
        try {
            Intent intent = new Intent();
            String manufacturer = android.os.Build.MANUFACTURER;
            if ("xiaomi".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity"));
            } else if ("oppo".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity"));
            } else if ("vivo".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.vivo.permissionmanager", "com.vivo.permissionmanager.activity.BgStartUpManagerActivity"));
            } else if ("Letv".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.letv.android.letvsafe", "com.letv.android.letvsafe.AutobootManageActivity"));
            } else if ("Honor".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.optimize.process.ProtectActivity"));
            }

            List<ResolveInfo> list = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
            if  (list.size() > 0) {
                context.startActivity(intent);
                SharedPrefsHelper.getInstance(context).createBoolean(Constants.SharedPrefs.KEY_IS_FIRST_START, true);
            }
        } catch (Exception e) {
            Logger.e(e.getMessage());
        }
    }

    public static void showServiceNotification(Service context) {
        NotificationHelper.addServiceNotification(context);
    }


    public static void runService(Context context, Intent i) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            ContextCompat.startForegroundService(context, i);
        } else {
            context.startService(i);
        }
    }


}

package phonetoaster.com.bubbletranslate.helper;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;

import java.util.Date;
import java.util.Random;

import phonetoaster.com.bubbletranslate.receiver.AlarmBroadcastReceiver;

/**
 * healthHelm
 * File Created by vavru on 27.8.2015.
 * Copyright (c) 2015 CertiCon a.s. All rights reserved.
 */
public class AlarmHelper {

    private static int RESPAWN_TRESHOLD = 5 * 60 * 1000;
    private static int ALARM_ID = 111;
    private int randomInt;

    public AlarmHelper() {
        Random random = new Random();
        randomInt = random.nextInt();
    }

    public static void setUniqueAlarm(Context context) {
        try {
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent alarmIntent = new Intent(context, AlarmBroadcastReceiver.class);
            Random random = new Random();
            Integer randomInt = random.nextInt();
            PendingIntent pendingAlarmIntent = PendingIntent.getBroadcast(context, ALARM_ID, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            if(alarmManager != null) { //SECOND CONDITION AVOID TO SCHEDULE REMINDERS WITH OCCURRENCE IN THE PAST
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, RESPAWN_TRESHOLD, pendingAlarmIntent);
                } else
                    alarmManager.setExact(AlarmManager.RTC_WAKEUP, RESPAWN_TRESHOLD, pendingAlarmIntent);
            }
        } catch (Exception e) {
            ExceptionLogHelper.log(e);
        }
    }

    public static void setRepeatedAlarm(Context context) {
        try {
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent alarmIntent = new Intent(context, AlarmBroadcastReceiver.class);
            PendingIntent pendingAlarmIntent = PendingIntent.getBroadcast(context, ALARM_ID, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            if (alarmManager != null) {
                alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                        SystemClock.elapsedRealtime(),
                        RESPAWN_TRESHOLD,
                        pendingAlarmIntent);
            }
        } catch (Exception e) {
            ExceptionLogHelper.log(e);
        }
    }

}

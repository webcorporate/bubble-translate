package phonetoaster.com.bubbletranslate.helper;

import android.content.Context;
import android.content.Intent;
import android.widget.ArrayAdapter;

import org.json.JSONArray;

import java.net.URLEncoder;

import okhttp3.Callback;
import okhttp3.Response;
import phonetoaster.com.bubbletranslate.R;
import phonetoaster.com.bubbletranslate.common.Constants;
import phonetoaster.com.bubbletranslate.model.LastTranslation;
import phonetoaster.com.bubbletranslate.model.Translation;
import phonetoaster.com.bubbletranslate.service.ResultService;

public class TranslationHelper {

    public static String getLangCodeBasedOnIndex(int indexPosition, Context context) {
        try {
            String langCodeForReturn = null;
            ArrayAdapter<CharSequence> langCodesArr = ArrayAdapter.createFromResource(context,
                    R.array.translation_lang_values, android.R.layout.simple_spinner_item);
            if(langCodesArr.getItem(indexPosition) != null)
                langCodeForReturn =  langCodesArr.getItem(indexPosition).toString();
            return langCodeForReturn;
        } catch (Exception e) {
            ExceptionLogHelper.log(e);
            return null;
        }
    }

    public static String getLangNameBasedOnIndex(int indexPosition, Context context) {
        try {
            String langNameForReturn = null;
            ArrayAdapter<CharSequence> langCodesArr = ArrayAdapter.createFromResource(context,
                    R.array.translation_lang_items, android.R.layout.simple_spinner_item);
            if(langCodesArr.getItem(indexPosition) != null)
                langNameForReturn =  langCodesArr.getItem(indexPosition).toString();
            return langNameForReturn;
        } catch (Exception e) {
            ExceptionLogHelper.log(e);
            return null;
        }
    }

    public static int getLangIndexByLangCode(String langCode, String type, Context context) {
        try {
            int langCodeIndex = 0;
            String[] langCodesArr = null;
            switch (type){
                case Constants.Global.LANG_SOURCE:
                    langCodesArr = context.getResources().getStringArray(R.array.translation_lang_values);
                    break;
                case Constants.Global.LANG_TRANSLATION:
                    langCodesArr = context.getResources().getStringArray(R.array.translation_lang_to_values);
                    break;
            }
            if (langCodesArr != null) {
                for (int i=0; i<langCodesArr.length; i++) {
                    String actualLangCode = langCodesArr[i];
                    if(actualLangCode.equals(langCode)) {
                        langCodeIndex = i;
                    }
                }
            }
            return langCodeIndex;
        } catch (Exception e) {
            ExceptionLogHelper.log(e);
            return 0;
        }
    }

    public static void translateGivenWordsOnline(final String sourceText, String sourceLang, String destLang, Callback callback) throws Exception {
        //TODO: MOVE TO SINGLETON
        //TODO: PASS LANG PARAMS TO URL CONST
        String encodedSourceText = URLEncoder.encode(sourceText, Constants.Global.TRANSLATE_TEXT_ENCODING);
        String urlToProcess = Constants.Global.TRANSLATE_URL_ENDPOINT
                + Constants.Global.URL_KEY_RESPONSE_TYPE
                + Constants.Global.URL_KEY_SOURCE_LANGUAGE + sourceLang
                + Constants.Global.URL_KEY_TRANSLATION_LANGUAGE + destLang
                + Constants.Global.URL_KEY_QUERY + encodedSourceText
                + Constants.Global.URL_KEY_INPUT_TEXT_ENCODING + Constants.Global.TRANSLATE_TEXT_ENCODING
                + Constants.Global.URL_KEY_OUTPUT_TEXT_ENCODING + Constants.Global.TRANSLATE_TEXT_ENCODING;



        /**************************************************
         sl - source language code (auto for autodetection)
         tl - translation language
         q - source text / word
         ie - input encoding (a guess)
         oe - output encoding (a guess)
         dt - may be included more than once and specifies what to return in the reply.
         Here are some values for dt. If the value is set, the following data will be returned:

         t - translation of source text
         at - alternate translations
         rm - transcription / transliteration of source and translated texts
         bd - dictionary, in case source text is one word (you get translations with articles, reverse translations, etc.)
         md - definitions of source text, if it's one word
         ss - synonyms of source text, if it's one word
         ex - examples
         rw - See also list.
         ***************************************************/
        HttpHelper httpHelper = new HttpHelper();
        httpHelper.doRequest(urlToProcess, callback);
    }

    public static Translation getTranslationFromResponse(Response response, Context context) {
        Translation tr = new Translation();
        try {
            String jsonString = new String(response.body().string().getBytes(Constants.Global.TRANSLATE_TEXT_ENCODING));
            //TODO: COVERT TO GSON  MODEL
            // ERASE ALL ITEMS BEFORE BEGIN OF THE TRANSLATION
            String sourceText = null;
            String translatedText = null;
            String sourceTextLang = null;
            JSONArray translatedResponsejsonArray = new JSONArray(jsonString);
            JSONArray ja1 = new JSONArray(translatedResponsejsonArray.get(0).toString());
            JSONArray ja2 = new JSONArray(ja1.get(0).toString());
            sourceText = ja2.get(1).toString();
            translatedText = ja2.get(0).toString();
            tr.setSourceText(sourceText);
            tr.setTranslatedText(translatedText);
        } catch (Exception e) {
            ToastHelper.showToastMessage(context,
                    context.getString(R.string.error_during_parsing_response),
                    Constants.Global.DEBUG);
            TrackingEventLogHelper.logException(e, Constants.Global.EXCEPTION,
                    context.getResources().getString(R.string.error_during_parsing_response), true);
        }
        return tr;
    }


    public static void displayTranslation(Context context, String sourceText, String translatedText, Boolean shouldSaveToLastTranslation) throws Exception {
        try {
            Translation.getInstance().setSourceText(sourceText);
            Translation.getInstance().setTranslatedText(translatedText);
            String notificationType = CommonHelper.getNotificationType(context);

            if(shouldSaveToLastTranslation) {
                saveToLastTranslations(Translation.getInstance(), context);
            }
            switch (notificationType) {
                case Constants.TranslationMode.FLOATING_WINDOW:
                    Intent resultServiceIntent = new Intent(context, ResultService.class);
                    resultServiceIntent.putExtra(Constants.Global.KEY_SHOULD_RELOAD_TRANSLATION, shouldSaveToLastTranslation);
                    context.startService(resultServiceIntent);
                    break;
                case Constants.TranslationMode.NOTIFICATION:
                    NotificationHelper.showNotification(sourceText, translatedText, context);
                    break;
                case Constants.TranslationMode.TOAST_MESSAGE:
                    ToastHelper.showToastMessage(context, translatedText, false);
                    break;
            }
        } catch (Exception e) {
            ExceptionLogHelper.log(e);
        }
    }

    private static void saveToLastTranslations(Translation tr, Context mContext) {
        //SAVE TO LAST TRANSLATIONS
        AppDatabase appDb = AppDatabase.getAppDatabase(mContext); //MOVE TO CONTAINER OR SINGLETON
        LastTranslation lstTr = new LastTranslation(tr.getSourceText(), tr.getTranslatedText());
        appDb.daoInterfaceLastTranslation().insertAll(lstTr);
    }
}

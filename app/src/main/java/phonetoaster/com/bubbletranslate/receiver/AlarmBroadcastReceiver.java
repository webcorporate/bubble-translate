package phonetoaster.com.bubbletranslate.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;

import phonetoaster.com.bubbletranslate.helper.ExceptionLogHelper;
import phonetoaster.com.bubbletranslate.service.ClipboardService;

public class AlarmBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            // Device battery life will be significantly affected by the use of this API.
            if(pm!=null) {
                PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,"TAG");
                // Acquire the lock
                wl.acquire(60*1000L /*10 minutes*/);
                //Release the lock
                wl.release();
            }
            this.triggerActionBasedOnTheAlarmType(context);
        } catch (Exception e) {
            ExceptionLogHelper.log(e);
        }
    }


    private void triggerActionBasedOnTheAlarmType(Context context) {
        Intent intentToOpen = new Intent(context, ClipboardService.class);
        context.startService(intentToOpen);
    }
}

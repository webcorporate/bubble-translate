package phonetoaster.com.bubbletranslate.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.orhanobut.logger.Logger;

import phonetoaster.com.bubbletranslate.common.Constants;
import phonetoaster.com.bubbletranslate.helper.AllListenerHelper;
import phonetoaster.com.bubbletranslate.helper.ToastHelper;
import phonetoaster.com.bubbletranslate.helper.TrackingEventLogHelper;

/**
 * Created by george on 06/07/16.
 */
public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            Logger.d("BootReceiver onReceive registered");
            if(Constants.Global.DEBUG)
                ToastHelper.showToastMessage(context, "BootReceiver", Constants.Global.DEBUG);
            AllListenerHelper.initializeListeners(context);
        } catch (Exception e) {
            TrackingEventLogHelper.logException(e, Constants.Global.EXCEPTION,
                    Constants.ExceptionMessage.EXC_CANNOT_CANNOT_PROCESS_REBOOT_RECEIVER, true);
        }
    }

}
package phonetoaster.com.bubbletranslate.receiver;

import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;

import phonetoaster.com.bubbletranslate.R;
import phonetoaster.com.bubbletranslate.common.Constants;
import phonetoaster.com.bubbletranslate.helper.AppDatabase;
import phonetoaster.com.bubbletranslate.helper.SyncDataHelper;
import phonetoaster.com.bubbletranslate.helper.ToastHelper;
import phonetoaster.com.bubbletranslate.helper.TrackingEventLogHelper;
import phonetoaster.com.bubbletranslate.model.LastTranslation;
import phonetoaster.com.bubbletranslate.model.Translation;

/**
 * Created by george on 06/07/16.
 */
public class NotificationReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            String operationType = intent.getStringExtra(Constants.Global.OPERATION_TYPE);
            String translation = intent.getStringExtra(Constants.Global.TRANSLATION);
            switch (operationType) {
                case Constants.Global.OPERATION_TYPE_COPY:
                    doCopy(translation, context);
                    break;
                case Constants.Global.OPERATION_TYPE_SAVE:
                    String sourceText = intent.getStringExtra(Constants.Global.SOURCE_TEXT);
                    doSave(sourceText, translation, context);
                    break;
            }
        } catch (Exception e) {
            TrackingEventLogHelper.logException(e, Constants.Global.EXCEPTION,
                    Constants.ExceptionMessage.EXC_CANNOT_CANNOT_PROCESS_REBOOT_RECEIVER, true);
        }
    }

    private void doCopy(String translation, Context context) {
        if(translation  != null){
            //NOTE: Label param in ClipData.newPlainText helps to avoid translate already translated text copied to clipboard
            ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText(Constants.Global.TRANSLATION, translation);
            if(clipboard != null) {
                clipboard.setPrimaryClip(clip);
            }
            this.dismissNotification(context);
            this.showToastCopied(context);
        }
    }

    private void doSave(String source, String translation, Context context) {
        AppDatabase appDb = AppDatabase.getAppDatabase(context); //MOVE TO CONTAINER OR SINGLETON
        Translation tr = new Translation(source, translation, null, null);
        LastTranslation lstTr = new LastTranslation(source, translation);
        appDb.daoInterfaceTranslation().insertAll(tr);
        this.showToastSaved(context);
        SyncDataHelper.broadcastGlobalUpdate(context);
    }

    private void dismissNotification(Context context) {
        //This is used to close the notification tray
        Intent it = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        context.sendBroadcast(it);
    }

    private void showToastCopied(Context context) {
        ToastHelper.showToastMessage(context, context.getString(R.string.translation_copied), Constants.Global.DEBUG);
    }

    private void showToastSaved(Context context) {
        ToastHelper.showToastMessage(context, R.string.translation_saved, Constants.Global.DEBUG);
    }

}
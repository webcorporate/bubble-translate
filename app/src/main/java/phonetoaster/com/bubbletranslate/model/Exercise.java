package phonetoaster.com.bubbletranslate.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.RoomWarnings;
import android.arch.persistence.room.Update;
import android.content.Context;
import android.os.AsyncTask;

import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import phonetoaster.com.bubbletranslate.container.Container;
import phonetoaster.com.bubbletranslate.helper.ExceptionLogHelper;

/**
 * Created by george on 22/09/16.
 */
@Entity(tableName = "exercise")
public class Exercise  {

    @PrimaryKey (autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;
    @ColumnInfo(name = "translationId")
    private Integer translationId;
    @ColumnInfo(name = "translationSessionId") //TODO: ADD DEFAULT VALUE
    private Integer translationSessionId;
    @ColumnInfo(name = "result")
    private Integer result;

    private static Exercise instance;

    public Exercise() {
        this.translationId = null;
        this.result = null;
    }

    @Ignore
    public Exercise(Integer translationId, Integer result, Integer translationSessionId) {
        this.translationId = translationId;
        this.result = result;
        this.translationSessionId = translationSessionId;
    }

    public static synchronized Exercise getInstance() {
        if (instance == null) {
            instance = new Exercise();
        }
        return instance;
    }

    public static List<Exercise> getAll() {
        List<Exercise> list = new ArrayList<>();
        try {
            list =  new GetAllAsyncTask().execute().get();
        } catch (InterruptedException | ExecutionException e) {
            ExceptionLogHelper.log(e);
        }
        return list;
    }

    public static Integer getCountDistinct() {
        Exercise.DaoInterface daoInterface = Container.getInstance().getDatabaseManager().daoInterfaceExercise();
        return  daoInterface.countDistinct().size();

    }

    public void save() {
        Exercise.DaoInterface daoInterface = Container.getInstance().getDatabaseManager().daoInterfaceExercise();
        daoInterface.insert(this);
        //this.setId(messageID);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTranslationId() {
        return translationId;
    }

    public void setTranslationId(Integer translationId) {
        this.translationId = translationId;
    }

    public Integer getTranslationSessionId() {
        return translationSessionId;
    }

    public void setTranslationSessionId(Integer translationSessionId) {
        this.translationSessionId = translationSessionId;
    }

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public Double getAverageScoreForCard(int translationId) {
        Exercise.DaoInterface daoInterface = Container.getInstance().getDatabaseManager().daoInterfaceExercise();
        return daoInterface.avgOfGivenWord(translationId);
    }

    public static class GetAllAsyncTask extends AsyncTask<Void, Void, List<Exercise>> {

        @Override
        protected void onPreExecute() {
            Logger.d("onPreExecute");
            super.onPreExecute();
        }

        @Override
        protected List<Exercise> doInBackground(Void... params) {
            Logger.d("doInBackground");
            List<Exercise> result = new ArrayList<>();
            try {
                Exercise.DaoInterface daoInterface = Container.getInstance().getDatabaseManager().daoInterfaceExercise();
                result = daoInterface.getAll();

            } catch (Exception e) {
                ExceptionLogHelper.log(e);
            }
            return result;
        }
    }

    @Dao
    public interface DaoInterface {

        @Query("SELECT * FROM exercise")
        List<Exercise> getAll();

        @SuppressWarnings(RoomWarnings.CURSOR_MISMATCH)
        @Query("SELECT DISTINCT translationSessionId from exercise")
        List<Integer> countDistinct();

        @Query("SELECT AVG(result) from exercise WHERE translationId =:id")
        double avgOfGivenWord(Integer id);

        @Query("SELECT AVG(result) from exercise WHERE translationSessionId =:id")
        double avgOfGivenSession(Integer id);

        @Query("SELECT COUNT(*) from exercise WHERE translationId =:id")
        int countOfGivenWord(Integer id);

        @Query("SELECT MAX(translationSessionId) from exercise")
        int getLastSessionId();

        @Insert
        void insertAll(Exercise... translations);

        @Insert
        void insert(Exercise item);

        @Update
        void update(Exercise translation);

        @Delete
        void delete(Exercise translation);
    }
}

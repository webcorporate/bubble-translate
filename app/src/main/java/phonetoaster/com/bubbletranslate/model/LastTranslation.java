package phonetoaster.com.bubbletranslate.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import android.os.AsyncTask;

import com.orhanobut.logger.Logger;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import phonetoaster.com.bubbletranslate.container.Container;
import phonetoaster.com.bubbletranslate.helper.ExceptionLogHelper;

/**
 * Created by george on 22/09/16.
 */
@Entity(tableName = "last_translation")
public class LastTranslation implements Serializable {

    @PrimaryKey (autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;
    @ColumnInfo(name = "sourceText")
    private String sourceText;
    @ColumnInfo(name = "translatedText")
    private String translatedText;
    private static LastTranslation instance;
    @ColumnInfo(name = "isSavedPhrase")
    private Boolean isSavedPhrase = false;

    public LastTranslation() {
        this.sourceText = null;
        this.translatedText = null;
    }

    @Ignore
    public LastTranslation(String sourceText, String translatedText) {
        this.sourceText = sourceText;
        this.translatedText = translatedText;
    }

    public static synchronized LastTranslation getInstance() {
        if (instance == null) {
            instance = new LastTranslation();
        }
        return instance;
    }

    public static List<LastTranslation> getAll() {
        List<LastTranslation> list = new ArrayList<>();
        try {
            list =  new GetAllAsyncTask().execute().get();
        } catch (InterruptedException | ExecutionException e) {
            ExceptionLogHelper.log(e);
        }
        return list;
    }

    public String getSourceText() {
        return sourceText;
    }

    public void setSourceText(String sourceText) {
        this.sourceText = sourceText;
    }

    public String getTranslatedText() {
        return translatedText;
    }

    public void setTranslatedText(String translatedText) {
        this.translatedText = translatedText;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getCount() {
        LastTranslation.DaoInterface daoInterface = (DaoInterface) Container.getInstance().getDatabaseManager().daoInterfaceLastTranslation();
        return daoInterface.count();
    }

    public Boolean getSavedPhrase() {
        return isSavedPhrase;
    }

    public void setSavedPhrase(Boolean savedPhrase) {
        isSavedPhrase = savedPhrase;
    }


    public static class GetAllAsyncTask extends AsyncTask<Void, Void, List<LastTranslation>> {

        @Override
        protected void onPreExecute() {
            Logger.d("onPreExecute");
            super.onPreExecute();
        }

        @Override
        protected List<LastTranslation> doInBackground(Void... params) {
            Logger.d("doInBackground");
            List<LastTranslation> result = new ArrayList<>();
            try {
                LastTranslation.DaoInterface daoInterface = (DaoInterface) Container.getInstance().getDatabaseManager().daoInterfaceLastTranslation();
                result = daoInterface.getAll();

            } catch (Exception e) {
                ExceptionLogHelper.log(e);
            }
            return result;
        }
    }

    @Dao
    public interface DaoInterface {

        @Query("SELECT * FROM last_translation ORDER BY id DESC")
        List<LastTranslation> getAll();

        @Query("SELECT COUNT(*) from last_translation")
        int count();

        @Query("SELECT COUNT(*) from last_translation WHERE sourceText =:source")
        int countOfGivenWord(String source);

        @Insert
        void insertAll(LastTranslation... lastTranslation);

        @Update
        void update(LastTranslation lastTranslation);

        @Delete
        void delete(LastTranslation lastTranslation);
    }
}

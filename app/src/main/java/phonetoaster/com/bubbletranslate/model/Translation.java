package phonetoaster.com.bubbletranslate.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import android.os.AsyncTask;

import com.orhanobut.logger.Logger;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import phonetoaster.com.bubbletranslate.container.Container;
import phonetoaster.com.bubbletranslate.helper.ExceptionLogHelper;

/**
 * Created by george on 22/09/16.
 */
@Entity(tableName = "translation")
public class Translation implements Serializable {

    @PrimaryKey (autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;
    @ColumnInfo(name = "sourceText")
    private String sourceText;
    @ColumnInfo(name = "translatedText")
    private String translatedText;
    @ColumnInfo(name = "langFrom")
    private String langFrom;
    @ColumnInfo(name = "langTo")
    private String langTo;
    @ColumnInfo(name = "favorite")
    private Boolean favorite = false;
    private static Translation instance;

    public Translation() {
        this.sourceText = null;
        this.translatedText = null;
    }

    @Ignore
    public Translation(String sourceText, String translatedText, String langFrom, String langTo) {
        this.sourceText = sourceText;
        this.translatedText = translatedText;
        this.langFrom = langFrom;
        this.langTo = langTo;
    }

    public static synchronized Translation getInstance() {
        if (instance == null) {
            instance = new Translation();
        }
        return instance;
    }

    public static List<Translation> getAll() {
        List<Translation> list = new ArrayList<>();
        try {
            list =  new GetAllAsyncTask().execute().get();
        } catch (InterruptedException | ExecutionException e) {
            ExceptionLogHelper.log(e);
        }
        return list;
    }

    public String getSourceText() {
        return sourceText;
    }

    public void setSourceText(String sourceText) {
        this.sourceText = sourceText;
    }

    public String getTranslatedText() {
        return translatedText;
    }

    public void setTranslatedText(String translatedText) {
        this.translatedText = translatedText;
    }

    public String getLangFrom() {
        return langFrom;
    }

    public void setLangFrom(String langFrom) {
        this.langFrom = langFrom;
    }

    public String getLangTo() {
        return langTo;
    }

    public void setLangTo(String langTo) {
        this.langTo = langTo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }

    public Integer getCount() {
        Translation.DaoInterface daoInterface = Container.getInstance().getDatabaseManager().daoInterfaceTranslation();
        return daoInterface.count();
    }

    public static class GetAllAsyncTask extends AsyncTask<Void, Void, List<Translation>> {

        @Override
        protected void onPreExecute() {
            Logger.d("onPreExecute");
            super.onPreExecute();
        }

        @Override
        protected List<Translation> doInBackground(Void... params) {
            Logger.d("doInBackground");
            List<Translation> result = new ArrayList<>();
            try {
                Translation.DaoInterface daoInterface = Container.getInstance().getDatabaseManager().daoInterfaceTranslation();
                result = daoInterface.getAll();

            } catch (Exception e) {
                ExceptionLogHelper.log(e);
            }
            return result;
        }
    }

    @Dao
    public interface DaoInterface {

        @Query("SELECT * FROM translation ORDER BY id DESC")
        List<Translation> getAll();

        @Query("SELECT COUNT(*) from translation")
        int count();

        @Query("SELECT COUNT(*) from translation WHERE sourceText =:source")
        int countOfGivenWord(String source);

        @Insert
        void insertAll(Translation... translations);

        @Update
        void update(Translation translation);

        @Delete
        void delete(Translation translation);
    }
}

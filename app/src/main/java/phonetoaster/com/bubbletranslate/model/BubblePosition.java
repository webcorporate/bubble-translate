package phonetoaster.com.bubbletranslate.model;

import android.content.Context;

import phonetoaster.com.bubbletranslate.R;
import phonetoaster.com.bubbletranslate.common.Constants;
import phonetoaster.com.bubbletranslate.helper.CommonHelper;

/**
 * Created by george on 22/09/16.
 */
public class BubblePosition {

    private int x;
    private int y;

    public BubblePosition(Context context) {
        String positionFromSharedPrefs = this.getDefPosition(context);
        Integer screenWidth = CommonHelper.getScreenSize(Constants.Global.SCREEN_WIDTH, context);
        Integer screenHeight = CommonHelper.getScreenSize(Constants.Global.SCREEN_HEIGHT, context);
        if(positionFromSharedPrefs.equals(context.getString(R.string.CODE_LEFT))) {
            this.x = calculateXPosition(screenWidth, context.getString(R.string.CODE_LEFT), context);
            this.y = Constants.Global.BUBBLE_DEF_POSITION_PADDING_BOTTOM_LEFT;
        }
        if(positionFromSharedPrefs.equals(context.getString(R.string.CODE_RIGHT))) {
            this.x = calculateXPosition(screenWidth, context.getString(R.string.CODE_RIGHT), context);
            this.y = screenHeight - Constants.Global.BUBBLE_DEF_POSITION_PADDING_BOTTOM_RIGHT;
        }

    }

    private Integer calculateXPosition(Integer width, String type, Context context) {
        Integer xToReturn = 0;
        if(type.equals(context.getString(R.string.CODE_LEFT))) {
            xToReturn = (width/100) * Constants.Global.BUBBLE_DEF_POSITION_SIDE_PADDING_PERCENT;
        }
        if(type.equals(context.getString(R.string.CODE_RIGHT))) {
            xToReturn = width - ((width/60) * Constants.Global.BUBBLE_DEF_POSITION_SIDE_PADDING_PERCENT);
        }
        return xToReturn;
    }

    private String getDefPosition(Context context) {
        return Preference.getInstance(context).getBubblePosition();
    }

    public Integer getXCoords() {
        return this.x;
    }

    public Integer getYCoords() {
        return this.y;
    }

}

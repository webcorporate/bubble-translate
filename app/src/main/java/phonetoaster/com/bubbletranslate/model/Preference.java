package phonetoaster.com.bubbletranslate.model;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.orhanobut.logger.Logger;

import phonetoaster.com.bubbletranslate.R;
import phonetoaster.com.bubbletranslate.common.Constants;

public class Preference {

    private SharedPreferences sp;
    private Context mContext;
    private static Preference instance;

    public static synchronized Preference getInstance(Context ctx) {
        if (instance == null) {
            instance = new Preference(ctx);
        }
        return instance;
    }

    public Preference(Context ctx) {
        sp = PreferenceManager.getDefaultSharedPreferences(ctx);
        mContext = ctx;
        //TODO, CHANGE DEFAULT VALUES TO CONST INSTEAD OF THE HARDCODED STRINGS
    }

    public boolean isAppEnabled() {
        return sp.getBoolean(Constants.Global.PREFERENCES_APP_ENABLED, true);
    }

    public String getTranslationType() {
        return sp.getString(Constants.Global.PREFERENCES_TRANSLATION_TYPE, Constants.TranslationMode.FLOATING_WINDOW);
    }

    public void setTranslationType(String notificationType) {
        Logger.d("Previous translation type "+ getTranslationType());
            instance.sp.edit()
            .putString(Constants.Global.PREFERENCES_TRANSLATION_TYPE, notificationType)
            .apply();

        Logger.d("Actual translation type "+ getTranslationType());
    }

    public String getSourceLang() {
        return sp.getString(Constants.Global.PREFERENCES_TRANSLATION_SOURCE_LANG, mContext.getString(R.string.CODE_AUTO));
    }

    public String getDestLang() {
        return sp.getString(Constants.Global.PREFERENCES_TRANSLATION_DEST_LANG, mContext.getString(R.string.CODE_EN));
    }

    public String getBubblePosition() {
        return sp.getString(Constants.Global.PREFERENCES_BUBBLE_POSITION, mContext.getString(R.string.CODE_RIGHT));
    }

    public void setAppVersion(String appVersion) {
        Logger.d("Previous version "+ getAppVersion());
        instance.sp.edit()
                .putString(Constants.SharedPrefs.KEY_APP_VERSION, appVersion)
                .apply();

        Logger.d("Actual version "+ getAppVersion());
    }

    public String getAppVersion() {
        return sp.getString(Constants.SharedPrefs.KEY_APP_VERSION, String.valueOf(0));
    }




}

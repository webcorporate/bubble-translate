package phonetoaster.com.bubbletranslate.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.view.View;
import android.widget.ImageView;

import com.orhanobut.logger.Logger;
import com.premnirmal.Magnet.IconCallback;
import com.premnirmal.Magnet.Magnet;
import com.txusballesteros.bubbles.BubbleLayout;
import com.txusballesteros.bubbles.BubblesManager;

import phonetoaster.com.bubbletranslate.R;
import phonetoaster.com.bubbletranslate.common.Constants;
import phonetoaster.com.bubbletranslate.helper.CommonHelper;
import phonetoaster.com.bubbletranslate.helper.ExceptionLogHelper;
import phonetoaster.com.bubbletranslate.helper.ToastHelper;
import phonetoaster.com.bubbletranslate.model.BubblePosition;

/**
 * Created by george on 06/07/16.
 */
public class BubbleService extends Service {

    private Context mContext;
    private Handler mHandler;
    private Runnable mRunnable;
    private String textToTranslate;
    private Magnet magnet;
    private Integer iconCoordsX, iconCoordsY;

    public BubbleService() {
        mContext = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        removeExistingBubble();
        if(intent != null) {
            Bundle b = intent.getExtras();
            textToTranslate = b.getString(Constants.Global.TRANSLATION);
            this.showBubble();
        }
        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        magnet = null;
        removeCallback();
    }

    private void removeCallback() {
        if(mRunnable != null && mHandler != null)
            mHandler.removeCallbacks(mRunnable);
    }

    private void removeExistingBubble() {
        try {
            if(magnet != null) {
                magnet.destroy();
                magnet = null;
            }
        } catch (Exception e) {
            Logger.e(e.getMessage());
        }
    }

    private void showBubble() {
        try {
            BubblePosition bp = new BubblePosition(this);
            ImageView iconView = new ImageView(this);
            iconView.setImageResource(R.mipmap.ic_launcher);
            magnet = Magnet.newBuilder(this)
                    // a view is required
                    .setIconView(iconView)
                    // all the parameters below are optional
                    .setIconCallback(mIconCallback)
                    .setHideFactor(0.2f)
                    .setShouldShowRemoveView(true)
                    .setRemoveIconShouldBeResponsive(true)
                    .setRemoveIconResId(R.drawable.ic_cancel)
                    .setRemoveIconShadow(R.drawable.bottom_shadow)
                    .setShouldStickToWall(true)
                    .setRemoveIconShouldBeResponsive(true)
                    //.setInitialPosition(50, 50)
                    .build();
            magnet.show();
            magnet.setPosition(bp.getXCoords(), bp.getYCoords());
            //setTimerForInactiveBubble();
        } catch (Exception e) {
            Logger.e(e.getMessage());
        }
    }

    private void setTimerForInactiveBubble() {
        try {
            Logger.d("setTimerForInactiveBubble");
            mHandler = new Handler();
            mRunnable = new Runnable() {
                @Override
                public void run() {
                    try {
                        Logger.d("Remove bubble from inactivity");
                        magnet.destroy();
                    } catch (Exception e) {
                        ExceptionLogHelper.log(e);
                    }
                }
            };
            mHandler.postDelayed(mRunnable,
                    Constants.Global.BUBBLE_DISMISS_DURATION);
        } catch (Exception e) {
            Logger.e(e.getMessage());
        }
    }

    private void bubbleClickCallback() {
        try {
            if (CommonHelper.isDeviceOnline(mContext)) {
                callTranslationService();
            }
            else {
                ToastHelper.showToastMessage(mContext, //TODO: ADD THE OFFLINE TRANSACTION
                    mContext.getResources().getString(R.string.no_internet_connectivity),
                    Constants.Global.DEBUG);
            }
            this.removeExistingBubble();
            this.stopSelf();
        } catch (Exception e) {
            ExceptionLogHelper.log(e);
        }
    }

    private IconCallback mIconCallback = new IconCallback() {
        @Override
        public void onFlingAway() {
            Logger.d("onFlingAway");
            magnet.destroy();
        }

        @Override
        public void onMove(float x, float y) {
            //Logger.d("onMove");
            iconCoordsX = (int) x;
            iconCoordsY = (int) y;
        }

        @Override
        public void onIconClick(View view, float v, float v1) {
            Logger.d("onIconClick");
            bubbleClickCallback();
        }

        @Override
        public void onIconDestroyed() {
            Logger.d("onIconDestroyed");
        }
    };

    private void callTranslationService() throws Exception {
        Intent myIntent = new Intent(mContext, TranslateService.class);
        //Intent myIntent = new Intent(mContext, ResultService.class); //TODO:REMOVE BEFORE PROD
        //Intent myIntent = new Intent(mContext, ResultServiceCard.class); //TODO:REMOVE BEFORE PROD
        myIntent.putExtra(Constants.Global.TRANSLATION, textToTranslate);
        myIntent.putExtra(Constants.Global.X_COORDS, iconCoordsX);
        myIntent.putExtra(Constants.Global.Y_COORDS, iconCoordsY);
        mContext.startService(myIntent);
    }

}

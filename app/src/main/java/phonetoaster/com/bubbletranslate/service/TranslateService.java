package phonetoaster.com.bubbletranslate.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;

import com.orhanobut.logger.Logger;
import com.premnirmal.Magnet.IconCallback;
import com.premnirmal.Magnet.Magnet;

import org.json.JSONArray;

import java.io.IOException;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;
import phonetoaster.com.bubbletranslate.R;
import phonetoaster.com.bubbletranslate.common.Constants;
import phonetoaster.com.bubbletranslate.helper.CommonHelper;
import phonetoaster.com.bubbletranslate.helper.ExceptionLogHelper;
import phonetoaster.com.bubbletranslate.helper.HttpHelper;
import phonetoaster.com.bubbletranslate.helper.ToastHelper;
import phonetoaster.com.bubbletranslate.helper.TranslationHelper;
import phonetoaster.com.bubbletranslate.model.Preference;
import phonetoaster.com.bubbletranslate.model.Translation;

/**
 * Created by george on 06/07/16.
 */
public class TranslateService extends Service {

    private Context mContext;
    private ProgressBar progressBar;
    private HttpHelper httpHelper;
    private String translatedText = null;
    private String sourceTextLang = null;
    private String jsonString = null;
    private JSONArray translatedResponsejsonArray = null;
    private Magnet magnetLoading;
    private Integer iconCoordsX, iconCoordsY;

    public TranslateService() {
        mContext = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        try {
            super.onDestroy();
            removeExistingBubble();
        } catch (Exception e) {
            ExceptionLogHelper.log(e);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            if (intent != null && intent.getExtras() != null) {
                CommonHelper.showServiceNotification(this);
                Bundle b = intent.getExtras();
                String textToTranslate = b.getString(Constants.Global.TRANSLATION);
                String notificationType = CommonHelper.getNotificationType(this);
                switch (notificationType) {
                    case Constants.TranslationMode.FLOATING_WINDOW:
                        iconCoordsX = b.getInt(Constants.Global.X_COORDS);
                        iconCoordsY = b.getInt(Constants.Global.Y_COORDS);
                        removeExistingBubble();
                        showLoadingBubble();
                    default:
                        translateGivenWordsOnline(textToTranslate);
                        break;
                }
            }
        } catch (Exception e) {
            ExceptionLogHelper.log(e);
        }
        return Service.START_NOT_STICKY;
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void translateGivenWordsOnline(final String sourceText) throws Exception {
        String sourceLang = Preference.getInstance(mContext).getSourceLang();
        String destLang = Preference.getInstance(mContext).getDestLang();
        TranslationHelper.translateGivenWordsOnline(sourceText, sourceLang, destLang, translateRequestCallback());
    }

    private Callback translateRequestCallback() {
        return new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                Logger.e("onFailure");
                Logger.e(e.getMessage());
                stopSelf();
                ToastHelper.showToastMessage(mContext, mContext.getString(R.string.error_during_translation_request), Constants.Global.DEBUG);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Logger.d("onResponse");
                try {
                    Translation tr = TranslationHelper.getTranslationFromResponse(response, mContext);
                    // CALL RESULT SERVICE
                    TranslationHelper.displayTranslation(getApplicationContext(), tr.getSourceText(), tr.getTranslatedText(),true);
                } catch (Exception e) {
                    ExceptionLogHelper.log(e);
                }
                stopSelf();
            }
        };
    }

    private void showLoadingBubble() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        if(inflater != null)
            progressBar = (ProgressBar) inflater.inflate(R.layout.progress_bar_loading, null);
        magnetLoading = Magnet.newBuilder(this)
                // a view is required
                .setIconView(progressBar)
                // all the parameters below are optional
                .setIconCallback(mLoadingIconCallback)
                .setHideFactor(0.2f)
                .setShouldShowRemoveView(true)
                .setRemoveIconShouldBeResponsive(true)
                .setRemoveIconResId(R.drawable.ic_cancel)
                .setRemoveIconShadow(R.drawable.bottom_shadow)
                .setShouldStickToWall(true)
                //.setInitialPosition(iconCoordsX, iconCoordsY) //TODO: DOES NOT SET PASSED CORDS CORRECTLY
                .build();
        magnetLoading.show();
        magnetLoading.setPosition(iconCoordsX, iconCoordsY);
    }

    private void removeExistingBubble() {
        try {
            if(magnetLoading != null) {

                progressBar.setVisibility(View.GONE);
                magnetLoading.destroy();
            }
        } catch (Exception e) {
            ExceptionLogHelper.log(e);
        }
    }

    private IconCallback mLoadingIconCallback = new IconCallback() {
        @Override
        public void onFlingAway() {
            stopSelf();
        }

        @Override
        public void onMove(float v, float v1) {
            iconCoordsX = (int) v;
            iconCoordsY = (int) v1;
        }

        @Override
        public void onIconClick(View view, float v, float v1) {
            Logger.d("onIconClick");
        }

        @Override
        public void onIconDestroyed() {
            stopSelf();
        }
    };

}

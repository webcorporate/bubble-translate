package phonetoaster.com.bubbletranslate.service;

import android.app.Service;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import com.daimajia.swipe.SwipeLayout;
import com.orhanobut.logger.Logger;
import com.transitionseverywhere.Rotate;
import com.transitionseverywhere.TransitionManager;


import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;
import phonetoaster.com.bubbletranslate.R;
import phonetoaster.com.bubbletranslate.activity.CustomFrameLayout;
import phonetoaster.com.bubbletranslate.activity.PreferenceActivity;
import phonetoaster.com.bubbletranslate.common.Constants;
import phonetoaster.com.bubbletranslate.helper.AppDatabase;
import phonetoaster.com.bubbletranslate.helper.CommonHelper;
import phonetoaster.com.bubbletranslate.helper.ExceptionLogHelper;
import phonetoaster.com.bubbletranslate.helper.SyncDataHelper;
import phonetoaster.com.bubbletranslate.helper.ToastHelper;
import phonetoaster.com.bubbletranslate.helper.TranslationHelper;
import phonetoaster.com.bubbletranslate.model.Preference;
import phonetoaster.com.bubbletranslate.model.Translation;

public class ResultService extends Service {

    private WindowManager windowManager;
    private AppCompatTextView textViewSourceText, textViewTranslatedText;
    private AppCompatEditText editTextSourceTextEt;
    private AppCompatImageView expandSourceIv;
    private ScrollView scrollViewViewSourceText, scrollViewTranslatedText;
    private Context mContext;
    private CustomFrameLayout mParentLayout;
    private FrameLayout frameLayout;
    private LayoutInflater mInflater;
    private AppCompatButton copyTranslationBtn, saveTranslationBtn;
    private WindowManager.LayoutParams params;
    private SwipeLayout overlaySwipeLayout;
    private ImageButton settingsBtn, closeBtn;
    private Spinner spinnerOriginal, spinnerTranslation;
    private Timer timer;
    private ProgressBar translationProgressBar;
    private boolean isRotated = false;
    private AppDatabase appDb;

    public ResultService() {
        mContext = this;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Logger.d("onDestroy");
        closeView();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            Logger.d("onStartCommand");
            initCommon();
            initViews();
            populateViews();
            initListeners();
            hideSourceTextPart();
            showView();
            setValuesToViews();
        } catch (Exception e) {
            ToastHelper.showToastMessage(mContext, mContext.getResources().getString(R.string.error_during_displaying_result), Constants.Global.DEBUG);
            //TrackingEventLogHelper.logException(e, Constants.Global.EXCEPTION, mContext.getResources().getString(R.string.error_during_displaying_result), true);
            ExceptionLogHelper.log(e);
        }

        return Service.START_STICKY;
    }

    private void closeView() {
        Logger.d("closeView");
        mParentLayout.setVisibility(View.GONE);
        stopSelf();//TODO: stopSelef shoud be removed?
    }

    private void initViews() {
        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mParentLayout = new CustomFrameLayout(mContext, windowManager);
        frameLayout = (FrameLayout) mInflater.inflate(R.layout.result_layout_new, mParentLayout);
        mParentLayout.setView(frameLayout);

        overlaySwipeLayout =  (SwipeLayout) mParentLayout.findViewById(R.id.swipe_layout);
        expandSourceIv =  (AppCompatImageView) mParentLayout.findViewById(R.id.expandSourceIv);

        //textViewSourceText = (AppCompatTextView) mParentLayout.findViewById(R.id.textViewSourceText);
        editTextSourceTextEt = (AppCompatEditText) mParentLayout.findViewById(R.id.editTextSourceText);
        textViewTranslatedText = (AppCompatTextView) mParentLayout.findViewById(R.id.textViewTranslatedText);
        scrollViewViewSourceText = (ScrollView) mParentLayout.findViewById(R.id.scrollViewSource);
        //scrollViewTranslatedText = (ScrollView) mParentLayout.findViewById(R.id.scrollView);
        copyTranslationBtn = (AppCompatButton) mParentLayout.findViewById(R.id.copyTranslationButton);
        saveTranslationBtn = (AppCompatButton) mParentLayout.findViewById(R.id.saveTranslationButton);
        settingsBtn = (ImageButton) mParentLayout.findViewById(R.id.settingsButtonTop);
        closeBtn = (ImageButton) mParentLayout.findViewById(R.id.closeButtonTop);
        spinnerOriginal = (Spinner) mParentLayout.findViewById(R.id.source_lang_spinner);
        spinnerTranslation = (Spinner) mParentLayout.findViewById(R.id.translation_lang_spinner);
        translationProgressBar = (ProgressBar) mParentLayout.findViewById(R.id.translationProcessingProgressBar);
        translationProgressBar.setVisibility(View.GONE);
        //set show mode.
        overlaySwipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);

        //add drag edge.(If the BottomView has 'layout_gravity' attribute, this line is unnecessary)
        overlaySwipeLayout.addDrag(SwipeLayout.DragEdge.Right, mParentLayout.findViewById(R.id.bottom_wrapper));
        //add drag edge.(If the BottomView has 'layout_gravity' attribute, this line is unnecessary)
        overlaySwipeLayout.addDrag(SwipeLayout.DragEdge.Left, mParentLayout.findViewById(R.id.bottom_wrapper));
        //scrollViewViewSourceText.setSmoothScrollingEnabled(false);
        //scrollViewViewSourceText.requestDisallowInterceptTouchEvent(false);
    }

    private void initCommon() {
        appDb = AppDatabase.getAppDatabase(mContext); //MOVE TO CONTAINER OR SINGLETON
    }

    private void populateViews() {
        populateSpinner();
    }

    private void hideSourceTextPart() {
        expandSourceIv.performClick();
    }

    private void initListeners() {
        ///////////////////
        //TOP BUTTONS
        ///////////////////
        settingsBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                openSettings();
            }
        });

        ///////////////////
        //SPINNER ORIGINAL
        ///////////////////
        spinnerOriginal.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                translateGivenWordsOnline();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Logger.d("onNothingSelected");
            }
        });

        ///////////////////
        //EXPAND SOURCE BUTTON
        ///////////////////
        expandSourceIv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final ViewGroup transitionsContainer = (ViewGroup) mParentLayout.findViewById(R.id.original_text);
                TransitionManager.beginDelayedTransition(transitionsContainer, new Rotate());
                expandSourceIv.setRotation(isRotated ? 0 : Constants.Global.TRANSITION_ROTATE_ANGLE);
                isRotated = !isRotated;
                CommonHelper.toogleView(scrollViewViewSourceText);
            }
        });

        ///////////////////
        //SPINNER TRANSLATION
        ///////////////////
        spinnerTranslation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                translateGivenWordsOnline();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Logger.d("onNothingSelected");
            }
        });

        ///////////////////
        //CLOSE BTN
        ///////////////////
        closeBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                closeView();
            }
        });

        ///////////////////
        //EDIT TEXT SRC
        ///////////////////
        editTextSourceTextEt.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Logger.d("requestFocus");
                editTextSourceTextEt.requestFocus();
            }
        });

        editTextSourceTextEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Logger.d("beforeTextChanged " +s);
                // Do some thing now
                //DEBUG HERE
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Logger.d("onTextChanged " +s);
                // Change the TextView background color
                editTextSourceTextEt.setBackgroundColor(Color.YELLOW);
                // The user is typing: reset already started timer (if existing)
                if (timer != null) {
                    timer.cancel();
                }


                // Get the EditText text and display it on TextView
                //editTextSourceTextEt.setText(editTextSourceTextEt.getText());
            }

            @Override
            public void afterTextChanged(Editable s) {
                Logger.d("afterTextChanged ");
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        translateGivenWordsOnline();
                    }
                }, 600); // 600ms delay before the timer executes the „run“ method from TimerTask
            }
        });

        ///////////////////
        //COPY TRANSLATION BTN
        ///////////////////
        copyTranslationBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Logger.d("Clicked on the copy translation layout");
                //TODO: move to separated method
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText(mContext.getResources().getString(R.string.translation_copied_to_clipboard), Translation.getInstance().getTranslatedText());
                if(clipboard != null) {
                    clipboard.setPrimaryClip(clip);
                    ToastHelper.showToastMessage(mContext,
                            mContext.getResources().getString(R.string.translation_copied_to_clipboard), false);
                    closeView();
                }

            }
        });

        ///////////////////
        //SAVE TRANSLATION BTN
        ///////////////////
        saveTranslationBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Logger.d("Clicked on the save translation layout");

                //GET SOURCE LANG CODE
                Integer indexSource = spinnerOriginal.getSelectedItemPosition();
                String sourceLangCode = TranslationHelper.getLangCodeBasedOnIndex(indexSource, mContext);
                //GET TRANSLATION LANG CODE
                Integer indexLangCode = spinnerTranslation.getSelectedItemPosition();
                String transationLangCode = TranslationHelper.getLangCodeBasedOnIndex(indexLangCode, mContext);
                //GET SOURCE TEXT
                String sourceTextToSave = editTextSourceTextEt.getText().toString();
                //GET TRANSLATION TEXT
                String transalionTextToSave = textViewTranslatedText.getText().toString();

                //SAVE VALUES
                //CHECK THAT WORD ALREADY DOES NOT EXIST IN THE DB
                if(appDb.daoInterfaceTranslation().countOfGivenWord(sourceTextToSave) > 0) {
                    ToastHelper.showToastMessage(mContext, mContext.getString(R.string.same_translation_already_save), true);
                } else {
                    Translation tr = new Translation(sourceTextToSave, transalionTextToSave, sourceLangCode, transationLangCode);
                    appDb.daoInterfaceTranslation().insertAll(tr);
                    ToastHelper.showToastMessage(mContext,
                            mContext.getResources().getString(R.string.translation_saved), false);
                    SyncDataHelper.broadcastGlobalUpdate(mContext);
                    closeView();
                }

            }
        });

        ///////////////////
        //OVERLAY SWIPE LAYOUT
        ///////////////////
        overlaySwipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onClose(SwipeLayout layout) {
                //when the SurfaceView totally cover the BottomView.
            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
                //you are swiping.
            }

            @Override
            public void onStartOpen(SwipeLayout layout) {
                //not implemented
            }

            @Override
            public void onOpen(SwipeLayout layout) {
                //when the BottomView totally show.
                Logger.d("onOpen");
                String direction = layout.getDragEdge().toString().toLowerCase();
                switch (direction) {
                    case Constants.Global.MOVE_RIGHT:
                        closeView();
                        break;
                    case Constants.Global.MOVE_LEFT:
                        closeView();
                        break;
                }
            }

            @Override
            public void onStartClose(SwipeLayout layout) {
                //not implemented
            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
                //when user's hand released.
            }
        });
    }

    private void populateSpinner() {
        // CREATE AN ARRAYADAPTER USING THE STRING ARRAY AND A DEFAULT SPINNER LAYOUT
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.translation_lang_items, android.R.layout.simple_spinner_item);
        // SPECIFY THE LAYOUT TO USE WHEN THE LIST OF CHOICES APPEARS
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // APPLY THE ADAPTER TO THE SPINNER
        spinnerOriginal.setAdapter(adapter);
        spinnerTranslation.setAdapter(adapter);
        // SET PRESELECTED VALUES (LANGUAGES FROM & TO) FROM APP SETTINGS
        String sourceLangCode = Preference.getInstance(mContext).getSourceLang();
        String destLangCode = Preference.getInstance(mContext).getDestLang();
        Integer indexSourceLang = TranslationHelper.getLangIndexByLangCode(sourceLangCode, Constants.Global.LANG_SOURCE, this);
        Integer indexDestLang = TranslationHelper.getLangIndexByLangCode(destLangCode, Constants.Global.LANG_SOURCE, this);
        spinnerOriginal.setSelection(indexSourceLang, false); //false keep onItemSelected from firing off on a newly instantiated Spinner
        spinnerTranslation.setSelection(indexDestLang, false); //false keep onItemSelected from firing off on a newly instantiated Spinner
    }

    private void setValuesToViews() {
        editTextSourceTextEt.setText(Translation.getInstance().getSourceText());
        textViewTranslatedText.setText(Translation.getInstance().getTranslatedText()); //TODO: CHECK TEXT TRIMMING (IS GOOGLE TRIMMING LONG RESULTS?)
    }

    private void showView() {
        try {
            int LAYOUT_FLAG;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                //WORKING ON CLEAN ANDROID
                LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
            } else {
                //WORKING ON XIAOMI
                LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_PHONE;
            }
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    LAYOUT_FLAG,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, //| WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                    PixelFormat.TRANSLUCENT);
            params.format =  PixelFormat.TRANSLUCENT;
            params.gravity = Gravity.START | Gravity.TOP;
            params.width = WindowManager.LayoutParams.MATCH_PARENT;
            params.height = WindowManager.LayoutParams.WRAP_CONTENT;
            params.packageName = getPackageName();
            params.x = 0;
            params.y = 0;
            mParentLayout.setParams(params);
            mParentLayout.setViewGroup(mParentLayout);
            windowManager.addView(mParentLayout, params);
            mParentLayout.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            ExceptionLogHelper.log(e);
        }
    }

    private void toggleProgress(Boolean showProgressBar) {
        try {
            if(showProgressBar) {
                textViewTranslatedText.setVisibility(View.GONE);
                translationProgressBar.setVisibility(View.VISIBLE);
            } else {
                textViewTranslatedText.setVisibility(View.VISIBLE);
                translationProgressBar.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            ExceptionLogHelper.log(e);
        }
    }

    private Callback translateRequestCallback() {
        return new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                Logger.e("onFailure");
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        toggleProgress(false);
                    }
                });
                ToastHelper.showToastMessage(mContext, mContext.getString(R.string.error_during_translation_request), Constants.Global.DEBUG);
                //TODO: SET PREVIOUS TRANSLATION IN CASE OF THE REQUEST FAILURE
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        toggleProgress(false);
                        Translation tr = TranslationHelper.getTranslationFromResponse(response, mContext);
                        textViewTranslatedText.setText(tr.getTranslatedText());
                        //UPDATE VIEWS
                        SyncDataHelper.broadcastGlobalUpdate(mContext);
                    }
                });
            }
        };
    }

    private void translateGivenWordsOnline() {
        String sourceLangCode = TranslationHelper.getLangCodeBasedOnIndex(spinnerOriginal.getSelectedItemPosition(), mContext);
        String destLangCode = TranslationHelper.getLangCodeBasedOnIndex(spinnerTranslation.getSelectedItemPosition(), mContext);
        String sourceText = editTextSourceTextEt.getText().toString();
        try {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    toggleProgress(true);
                }
            });
            TranslationHelper.translateGivenWordsOnline(sourceText, sourceLangCode, destLangCode, translateRequestCallback());
        } catch (Exception e) {
            ExceptionLogHelper.log(e);
        }
    }

    private void openSettings() {
        closeView();
        settingsBtn.setColorFilter(R.color.colorAccent, PorterDuff.Mode.SRC_IN);
        Intent settingsIntent = new Intent(mContext, PreferenceActivity.class);
        mContext.startActivity(settingsIntent);
    }

}


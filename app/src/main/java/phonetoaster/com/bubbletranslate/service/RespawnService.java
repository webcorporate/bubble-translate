package phonetoaster.com.bubbletranslate.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.view.View;
import android.widget.ImageView;

import com.orhanobut.logger.Logger;
import com.premnirmal.Magnet.IconCallback;
import com.premnirmal.Magnet.Magnet;

import phonetoaster.com.bubbletranslate.R;
import phonetoaster.com.bubbletranslate.common.Constants;
import phonetoaster.com.bubbletranslate.helper.CommonHelper;
import phonetoaster.com.bubbletranslate.helper.ToastHelper;
import phonetoaster.com.bubbletranslate.model.BubblePosition;

/**
 * Created by george on 06/07/16.
 */
public class RespawnService extends Service {

    private Context mContext;
    private Handler mHandler;
    private Runnable mRunnable;
    private String textToTranslate;
    private Magnet magnet;
    private Integer iconCoordsX, iconCoordsY;

    public RespawnService() {
        mContext = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return startId;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


}

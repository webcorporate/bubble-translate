package phonetoaster.com.bubbletranslate.service;

import android.app.Service;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Patterns;
import android.webkit.URLUtil;

import com.orhanobut.logger.Logger;

import org.apache.commons.lang3.math.NumberUtils;

import phonetoaster.com.bubbletranslate.R;
import phonetoaster.com.bubbletranslate.common.Constants;
import phonetoaster.com.bubbletranslate.helper.CommonHelper;
import phonetoaster.com.bubbletranslate.helper.NotificationHelper;
import phonetoaster.com.bubbletranslate.helper.ToastHelper;
import phonetoaster.com.bubbletranslate.model.Preference;

/**
 * Created by george on 06/07/16.
 */
public class ClipboardService extends Service {
    private ClipboardManager mClipboardManager;
    private Context mContext;
    private ClipData clipData;
    private ClipData.Item item;
    String previousText = "";
    static boolean bHasClipChangedListener = false;

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            initCommon();
        } catch (Exception e) {
            Logger.e(e.getMessage());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Logger.d("onDestroy");
        if (mClipboardManager != null) {
            //this.UnRegPrimaryClipChanged();
        }
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void initCommon() throws Exception {
        mContext = this;
        this.initClipboardManager();
        //Intent myIntent = new Intent(mContext, ResultServiceSurface.class);
        //mContext.startService(myIntent);
    }

    /*
    private void showResultLayout() {
        try {
            resultServiceIntent = new Intent(mContext, ResultService.class);
            startService(resultServiceIntent);
            WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
            LayoutInflater mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            LinearLayout mParentLayout = (LinearLayout) mInflater.inflate(R.layout.result_layout, null);
            RelativeLayout rl = (RelativeLayout) mParentLayout.findViewById(R.id.loading_progress_bar);
            rl.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            Logger.e(e.getMessage());
        }
    }
    */

    private void RegPrimaryClipChanged(){
        if(!bHasClipChangedListener){
            mClipboardManager.addPrimaryClipChangedListener(mOnPrimaryClipChangedListener);
            bHasClipChangedListener = true;
        }
    }
    private void UnRegPrimaryClipChanged(){
        if(bHasClipChangedListener){
            mClipboardManager.removePrimaryClipChangedListener(mOnPrimaryClipChangedListener);
            bHasClipChangedListener = false;
        }
    }

    private ClipboardManager.OnPrimaryClipChangedListener mOnPrimaryClipChangedListener =
            new ClipboardManager.OnPrimaryClipChangedListener() {
                @Override
                public void onPrimaryClipChanged() {
                    try {
                        // CHECK THAT APP IS ENABLED
                        if (Preference.getInstance(mContext).isAppEnabled()) { //TODO: MOVE TO HELPER METHOD
                            clipData = mClipboardManager.getPrimaryClip();
                            item = clipData.getItemAt(0);
                            String textFromClipboard = String.valueOf(item.getText());
                            String clipboardLabel = String.valueOf(clipData.getDescription().getLabel());
                            if (!clipboardLabel.equals(Constants.Global.TRANSLATION)
                                    && !previousText.equals(textFromClipboard) && !isBlockedTextToTranslate(textFromClipboard)) {
                                previousText = textFromClipboard; // Solves issue: Listener Change called two times issue
                                processTextFromClipboardBasedOnNotificationType(textFromClipboard);
                            } else
                                Logger.d("This is translation, do not copy them!");
                        } else {
                            Logger.e("App translation is disabled");
                        }
                    } catch (Exception e) {
                        Logger.e(e.getMessage());
                    }
                }
            };

    private void initClipboardManager() throws Exception {
        Logger.d("initClipboardManager");
        // TODO: Show an ongoing notification when this service is running.
        mClipboardManager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        this.RegPrimaryClipChanged();
    }

    private void processTextFromClipboardBasedOnNotificationType(String textToTranslate) throws Exception {
        Intent translationServiceIntent = new Intent(this, TranslateService.class);
        Intent resultServiceIntent = new Intent(this, ResultService.class);
        //STOP SERVICES WHICH CAN DISPLAY PROGRESS|RESULT TO AVOID ZOMBIE SERVICES
        stopService(translationServiceIntent);
        stopService(resultServiceIntent);

        String notificationType = CommonHelper.getNotificationType(this);
        switch (notificationType) {
            case Constants.TranslationMode.FLOATING_WINDOW:
                Intent bubbleServiceIntent = new Intent(this, BubbleService.class);
                bubbleServiceIntent.putExtra(Constants.Global.TRANSLATION, textToTranslate);
                startService(bubbleServiceIntent);
                break;
            case Constants.TranslationMode.NOTIFICATION:
                showProcessingToast(this, textToTranslate);
                translationServiceIntent.putExtra(Constants.Global.TRANSLATION, textToTranslate);
                translationServiceIntent.putExtra(Constants.Global.TRANSLATION_MODE, Constants.TranslationMode.NOTIFICATION);
                startService(translationServiceIntent);
                break;
            case Constants.TranslationMode.TOAST_MESSAGE:
                showProcessingToast(this, textToTranslate);
                translationServiceIntent.putExtra(Constants.Global.TRANSLATION, textToTranslate);
                translationServiceIntent.putExtra(Constants.Global.TRANSLATION_MODE, Constants.TranslationMode.TOAST_MESSAGE);
                startService(translationServiceIntent);
                break;
        }
    }

    private void showProcessingToast(Context context, String text) {
        ToastHelper.showToastMessage(context, this.getString(R.string.translating, text), Constants.Global.DEBUG);
    }
    /*
     Checks that given string is not number, URL, IP address, etc..
     Can be tested on the following strings
        //http://www.test.com
        //192.168.1.0
        //domain.com
        //test.com
        //Ahoj, jak se mas
     */
    private Boolean isBlockedTextToTranslate(String textToTranslate) throws Exception {
        Boolean isBlocked = true;
        if(!TextUtils.isEmpty(textToTranslate)
                && !TextUtils.isDigitsOnly(textToTranslate)
                && !URLUtil.isValidUrl(textToTranslate)
                && !android.util.Patterns.PHONE.matcher(textToTranslate).matches()
                && !NumberUtils.isNumber(textToTranslate)
                && !Patterns.EMAIL_ADDRESS.matcher(textToTranslate).matches()
                && !Patterns.IP_ADDRESS.matcher(textToTranslate).matches()
                && !Patterns.DOMAIN_NAME.matcher(textToTranslate).matches()) {
            isBlocked = false;
        }
        return isBlocked;
    }

    private void displayRelatedNotificationType(String textToTranslate, String translatedText) {
        switch (CommonHelper.getNotificationType(this)) {
            case Constants.TranslationMode.FLOATING_WINDOW:
                Logger.d(Constants.TranslationMode.FLOATING_WINDOW);
                //TODO: INITIALIZE BUBBLE FIRST IN THIS CASE
                break;
            case Constants.TranslationMode.NOTIFICATION:
                Logger.d(Constants.TranslationMode.NOTIFICATION);
                NotificationHelper.showNotification(textToTranslate, translatedText, this);
                break;
            case Constants.TranslationMode.TOAST_MESSAGE:
                Logger.d(Constants.TranslationMode.TOAST_MESSAGE);
                ToastHelper.showToastMessage(this, translatedText, Constants.Global.DEBUG);
                break;
        }
    }

}
